package ca.ualberta.entitylinking.utils;

public class Pair<S, T> {
	public S value1;
	public T value2;
	
	public Pair(S value1, T value2) {
		this.value1 = value1;
		this.value2 = value2;
	}
	
	public S getValue1() {
		return this.value1;
	}
	
	public T getValue2() {
		return this.value2;
	}
	
	public void setValue1(S value) {
		this.value1 = value;
	}
	
	public void setValue2(T value) {
		this.value2 = value;
	}
}
