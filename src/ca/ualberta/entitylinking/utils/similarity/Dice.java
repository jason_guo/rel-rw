package ca.ualberta.entitylinking.utils.similarity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import ca.ualberta.entitylinking.utils.similarity.index.NgramIndex;
import ca.ualberta.entitylinking.utils.similarity.index.Posting;
import ca.ualberta.entitylinking.utils.similarity.index.StringNgrams;

/**
 * 
 * Dice
 * 
 * implements the Dice's coefficient (see <a href="http://en.wikipedia.org/wiki/Dice's_coefficient">http://en.wikipedia.org/wiki/Dice's_coefficient</a>).
 * Dice can be used with n-gram indices to perform a <i>coarse search</i> (see <a href=http://portal.acm.org/citation.cfm?id=210499">Zobel and Dart's</a>  
 * paper).
 * 
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */
public class Dice {
	
	NgramIndex index;
	
	public Dice(){
		index = new NgramIndex();
	}
	
	public Dice(NgramIndex index){
		this.index = index;
	}

	public void addIndex(NgramIndex index){
		this.index.addIndex(index);
	}
	
	public static double score(StringNgrams s, StringNgrams t){
		
		if(s == null || t == null){
			return 0.0;
		}
				
		if(s.getNumberOfGrams() == 0 || t.getNumberOfGrams() == 0){
			return 0.0;
		}
		
		if(s.equals(t)){
			return 1.0;
		}
		
		int bothTotal=0;
		
		for(String gram : s.getGrams().keySet()){
			if(t.getGrams().containsKey(gram)){
				//System.out.println("Common: " + gram);
				bothTotal += Math.min(s.getGrams().get(gram), t.getGrams().get(gram));
			}
		}
		
		//System.out.println("s: " + s.getNumberOfGrams());
		//System.out.println("t: " + t.getNumberOfGrams());
		
		return (double) 2 * bothTotal / (s.getNumberOfGrams() + t.getNumberOfGrams());
	}
	
	public Map<StringNgrams, Double> score(StringNgrams s){
		
		if(index == null) return null;
		
		HashMap<StringNgrams,Double> scores = new HashMap<StringNgrams, Double>();
				
		for(String gram : s.getGrams().keySet()){
			List<Posting> postingList = index.getPostingList(gram);
			
			if(postingList == null){
				continue;
			}
			
			//System.out.println("Common: " + gram);
			
			for(Posting posting : postingList){
				StringNgrams t = (StringNgrams) posting.getObject();
				
				double gramScore = (double) 2 * Math.min(s.getGrams().get(gram),posting.getWeight());
				
				if(scores.containsKey(t)){
					Double partialScore = scores.get(t);
					scores.put(t, partialScore + gramScore);
				}else{
					scores.put(t, gramScore);
				}
			}
		}
				
		for(StringNgrams t : scores.keySet()){
			Double partialScore = scores.get(t);
			scores.put(t, partialScore / (s.getNumberOfGrams() + t.getNumberOfGrams()));
			//System.out.println("s: " + s.getNumberOfGrams());
			//System.out.println("t: " + t.getNumberOfGrams());
		}
		
		return scores;
	}
	
	public Map<StringNgrams, Double> getSuperStringsOf(StringNgrams s){
		
		if(index == null) return null;

		HashMap<StringNgrams,Double> scores = new HashMap<StringNgrams, Double>();
		
		boolean firstGram=true;
		
		for(String gram : s.getGrams().keySet()){
			
			List<Posting> postingList = index.getPostingList(gram);
					
			if(postingList == null){
				//System.out.println("Out: " + gram);
				
				if(firstGram){
					//If no string t (in the index) has the first gram, there is no superstring of s in the index. 
					return scores;
				}else{
					continue;
				}
				
			}
			
			//System.out.println("Common: " + gram);

			// In this is the first gram in s, just add partial scores.
			if(firstGram){
				firstGram=false;
				for(Posting posting : postingList){
					StringNgrams t = (StringNgrams) posting.getObject();
					double gramScore = (double) 2 * Math.min(s.getGrams().get(gram),posting.getWeight());
					scores.put(t, gramScore);
				}
				continue;
			}
			
			//From now on, we can only update or remove strings from scores.
			
			//Put the strings containing gram in a hash
			HashSet<StringNgrams> postingHash = new HashSet<StringNgrams>();
			for(Posting posting : postingList){
				StringNgrams t = (StringNgrams) posting.getObject();
				postingHash.add(t);
			}
			
			//Test whether previous scored string contain the new gram.
			for(StringNgrams t : scores.keySet()){
				if(postingHash.contains(t)){
					if(s.getGrams().get(gram) <= t.getGrams().get(gram)){

						// Compute the Dice's coefficient numerator
						Double partialScore = scores.get(t);
						double gramScore = (double) 2 * Math.min(s.getGrams().get(gram),t.getGrams().get(gram));
						scores.put(t, partialScore + gramScore);
						
					}else{
						// If string t presents less occurrences of gram than s, it is not a superstring of s.
						scores.remove(t);
					}
				}else{
					// If string t does not contain gram, it is not a superstring of s.
					scores.remove(t);
				}
			}
		}
				
		for(StringNgrams t : scores.keySet()){
			String formattedS = StringNgrams.formatString(s.getString());
			String formattedT = StringNgrams.formatString(t.getString());
			
			int index = formattedT.indexOf(formattedS);
			
			// If S is not a substring of T, ignore T.
			if(index < 0){
				scores.remove(t);
				continue;
			}
			
			// Compute the Dice's coefficient denominator
			Double partialScore = scores.get(t);
			scores.put(t, partialScore / (s.getNumberOfGrams() + t.getNumberOfGrams()));
		}
		
		return scores;

	}
	
	public static void main(String [] args){
		StringNgrams s = new StringNgrams(2,"Obama Jr.",null);
		StringNgrams t = new StringNgrams(2,"Barack Obama Jr.",null);

		System.out.println("[" + s + "] vs. [" + t + "]");
		System.out.println("Score: " + Dice.score(s, t));

		NgramIndex index = new NgramIndex();
		index.index(t);
		Dice dice = new Dice(index);

		Map<StringNgrams,Double> results = dice.score(s);
		System.out.println("Score: " + results.get(t));
		results = dice.getSuperStringsOf(s);
		System.out.println("Superstring: " + results.get(t));
		
	}

}
