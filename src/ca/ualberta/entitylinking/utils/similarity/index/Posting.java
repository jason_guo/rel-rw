package ca.ualberta.entitylinking.utils.similarity.index;

import java.io.Serializable;

/**
 * Posting
 * 
 * is an element in a posting list from a inverted index (see <a href=http://en.wikipedia.org/wiki/Inverted_index">http://en.wikipedia.org/wiki/Inverted_index</a>).
 * 
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class Posting implements Serializable {
	
	private static final long serialVersionUID = 872698445742185063L;
	protected Object object;
	protected double weight;
	
	public Posting(Object object, double weight){
		this.object = object;
		this.weight = weight;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	
	

}
