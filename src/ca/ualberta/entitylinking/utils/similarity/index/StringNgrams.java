package ca.ualberta.entitylinking.utils.similarity.index;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * StringNgrams
 * 
 * is a wrapper for a string to store its ngrams. It can also store an object that is somehow related to this string.
 * For example, one may store an {@link relibrary.data.Entity} along the string representing its name or alias. 
 *  
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class StringNgrams implements Serializable{

	private static final long serialVersionUID = -8558523483049700812L;
	protected int n;
	protected String string;
	protected Object object;
	
	protected Map<String,Integer> grams;
	protected int numberOfGrams;
	
	public StringNgrams(int n, String string, Object object){
		this.string = string;
		this.object = object;
		this.n = n;
		
		numberOfGrams=0;		
		
		grams = new HashMap<String,Integer>(5);
		
		extractNgrams(string);
	}
	
	public StringNgrams(int i, String mention) {
		this(i, mention, null);
	}

	public String toString(){
		return string;
	}
	
	private void extractNgrams(String s){

		s = formatString(s);
		
		char[] chars = s.toCharArray();
		numberOfGrams=0;
		
		//System.out.println("String: " + s);

		// Iterate over characters in s
		for(int i=0;i<=chars.length - n;i++){
			
			// Extract the ngram starting at character at position i
			char[] ngram = new char[n];
			for(int j=0;j<n;j++){
				ngram[j]=chars[i+j];
			}
			
			numberOfGrams++;
			
			String ngramStr = new String(ngram);
			//System.out.println("Ngram: [" + ngramStr + "]");
			// Update the count for this ngram 
			if(grams.containsKey(ngramStr)){
				Integer count = grams.get(ngramStr);
				count++;
				grams.put(ngramStr, count);
			}else{
				grams.put(ngramStr, 1);
			}
		}
	}	
	
	public static String formatString(String s){
		
		s = s.toLowerCase().trim();
		s = s.replaceAll("\\s+", " ");
		s = s.replaceAll("[^\\w ]", "");
		s = " " + s + " ";
		
		return s;
	}


	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public int getN() {
		return n;
	}

	public Map<String, Integer> getGrams() {
		return grams;
	}

	public int getNumberOfGrams() {
		return numberOfGrams;
	}

	
}
