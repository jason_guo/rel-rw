package ca.ualberta.entitylinking.utils.similarity.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * EqualityIndex
 * 
 * is a mapping from a string to a list of objects.
 * 
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */


public class EqualityIndex {

	protected Map<String,List<Posting>> index;

	public EqualityIndex(){
		index = new HashMap<String,List<Posting>>(1000);
	}

	public void index(String s, Object o){
		index(s,o,1L);		
	}
	
	public void index(String s, Object o, Long occurrences){

		List<Posting> postingList = null;

		if(index.containsKey(s)){
			postingList = index.get(s);
		}else{
			postingList = new ArrayList<Posting>();
			index.put(s, postingList);
		}
		
		Posting posting = new Posting(o, occurrences);
		postingList.add(posting);
		
	}

	public List<Posting> getPostingList(String s){
		return index.get(s);
	}

}

