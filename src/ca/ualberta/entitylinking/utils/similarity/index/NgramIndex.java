package ca.ualberta.entitylinking.utils.similarity.index;

import java.io.Serializable;
import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * NgramIndex
 * 
 * is an inverted index over the ngrams of a list of strings (see <a href=http://en.wikipedia.org/wiki/Inverted_index">http://en.wikipedia.org/wiki/Inverted_index</a>).
 * 
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class NgramIndex implements Serializable{
	
	private static final long serialVersionUID = -2851377814638605049L;
	protected Map<String,List<Posting>> index;
	
	public NgramIndex(){
		index = new HashMap<String,List<Posting>>(1000);
	}
	
	public Map<String,List<Posting>> getIndex(){
		return index;
	}
	
	public void addIndex(NgramIndex index){
		this.index.putAll(index.getIndex());
	}
	
	public void index(StringNgrams s){
		
		for(String gram : s.getGrams().keySet()){
			List<Posting> postingList = null;
			
			if(index.containsKey(gram)){
				postingList = index.get(gram);
			}else{
				postingList = new LinkedList<Posting>();
				index.put(gram, postingList);
			}
			
			Posting posting = new Posting(s, s.getGrams().get(gram));
			postingList.add(posting);
		}
	}
	
	public List<Posting> getPostingList(String gram){
		return index.get(gram);
	}
	
	public void readExternal(ObjectInput in) {
		
	}
	
	public void writeExternal(ObjectOutput out) {
		
	}
}
