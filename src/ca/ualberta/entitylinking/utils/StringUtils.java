package ca.ualberta.entitylinking.utils;

import java.io.StringReader;
import java.util.Arrays;

import javatools.parsers.PlingStemmer;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.util.Version;

/**
 * Mainly about the utils for string operations.
 * @author zhaochen
 *
 */
public class StringUtils {
	/**
	 * Use the StopAnalyzer.ENGLISH_STOP_WORDS_SET to check if the given term is a stop word.
	 * 
	 * @return
	 */
	public static boolean isStopWord(String word) {
		if (word == null || word.isEmpty())
			return true;
		
		CharArraySet stopWords = (CharArraySet)StopAnalyzer.ENGLISH_STOP_WORDS_SET;
		return stopWords.contains(word.toLowerCase());
	}
	
	/**
	 * Only works for ascii characters.
	 * 
	 * @param str
	 * @return
	 */
	public static String sort(String str) {
		if (str == null || str.isEmpty())
			return str;
		
		char[] chars = str.toCharArray();
		Arrays.sort(chars);
		
		return new String(chars);
	}
	
	/**
	 * Convert a word in plural format to its singular.
	 * 
	 * @param word
	 * @return
	 */
	public static String p2s(String word) {
		return PlingStemmer.stem(word);
	}
	
	public static boolean isPlural(String word) {
		return PlingStemmer.isPlural(word);
	}
	
	public static void main(String[] args) {
		System.out.println(p2s("men"));
	}
}