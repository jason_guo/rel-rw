package ca.ualberta.entitylinking.utils;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XmlProcessor {
	public XmlProcessor() {
		
	}
	
	public static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element element = (Element) nl.item(0);
			if (element.hasChildNodes())
				textVal = element.getFirstChild().getNodeValue();
		}
		
		return textVal;
	}
	
	public static int getIntValue(Element ele, String tagName) {
		return Integer.parseInt(getTextValue(ele, tagName));
	}
	
	public static String getAttribute(Element ele, String tagName, String attrName) {
		String value = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element element = (Element) nl.item(0);
			if (element.hasAttribute(attrName))
				value = element.getAttribute(attrName);
		}
		
		return value;
	}
	
	public static String getTextValue(Element ele, String tagName, String attrName, String attrValue) {
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl == null || nl.getLength() <= 0)
			return null;

		for (int i = 0; i < nl.getLength(); i++) {
			Element element = (Element) nl.item(i);
			if (element.hasAttribute(attrName)) {
				String value = element.getAttribute(attrName);
				if (value.equalsIgnoreCase(attrValue))
					return element.getTextContent();
			}
		}
		
		return null;
	}
	
}
