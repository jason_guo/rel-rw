package ca.ualberta.entitylinking.utils;

public class Rank<T extends Comparable<T>, V> implements Comparable<Rank<T, V>> {
	public T sim;
	public V obj;

	public Rank(T sim, V obj) {
		this.sim = sim;
		this.obj = obj;
	}

	@Override
	public int compareTo(Rank<T, V> target) {
		// TODO Auto-generated method stub
		if (sim.compareTo(target.sim) < 0)
			return 1;
		else if (sim.compareTo(target.sim) == 0)
			return 0;
		else
			return -1;
	}
}
