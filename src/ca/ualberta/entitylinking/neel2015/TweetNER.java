package ca.ualberta.entitylinking.neel2015;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import gate.*;
import gate.creole.*;
import gate.corpora.RepositioningInfo;

public class TweetNER {
	private static Logger LOGGER = LogManager.getLogger(TweetNER.class);
	private SerialAnalyserController controller;
	
	public static final String[] PR_NAMES = {
		"gate.creole.annotdelete.AnnotationDeletePR",
//		"gate.creole.tokeniser.DefaultTokeniser",
		"gate.twitter.tokenizer.TokenizerEN",
		"gate.creole.gazetteer.DefaultGazetteer",
		"gate.creole.splitter.SentenceSplitter",
//		"gate.creole.POSTagger",
		"gate.twitter.pos.POSTaggerEN",
		"gate.creole.ANNIETransducer",
		"gate.creole.orthomatcher.OrthoMatcher"
	};

	public TweetNER() {
		String gateHomePath = "/nfs/data1/zhaochen/entitylinking/data/gate8.1";
		String gateConfigPath = gateHomePath + "/gate.xml";
		String gatePluginHome = gateHomePath + "/plugins";

		System.setProperty("gate.home", gateHomePath);
		System.setProperty("gate.site.config", gateConfigPath);
		System.setProperty("gate.user.config", gateConfigPath);
		System.setProperty("gate.plugins.home", gatePluginHome);

		try {
			Gate.init();

			// Load ANNIE plugin
			File gateHome = Gate.getGateHome();
			File pluginsHome = new File(gateHome, "plugins");
			Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "ANNIE").toURI().toURL());
			Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Twitter").toURI().toURL());
			Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Stanford_CoreNLP").toURI().toURL());

			// create a serial analyser controller to run ANNIE with
			controller = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController",
					Factory.newFeatureMap(), Factory.newFeatureMap(), "ANNIE_" + Gate.genSym());
			// load each PR as defined in ANNIEConstants
			for (int i = 0; i < PR_NAMES.length; i++) {
				FeatureMap params = Factory.newFeatureMap(); // use default parameters
				ProcessingResource pr = (ProcessingResource) Factory.createResource(PR_NAMES[i], params);

				// add the PR to the pipeline controller
				controller.add(pr);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<TweetMention> annotate(String tweet) {
		FeatureMap params = Factory.newFeatureMap();
		params.put("stringContent", tweet);
		params.put("preserveOriginalContent", new Boolean(true));
		params.put("collectRepositioningInfo", new Boolean(true));
	    Corpus corpus = null;
	    Document doc = null;
	    try {
	    	corpus = Factory.newCorpus("corpus");
			doc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);
			corpus.add(doc);
			// tell the pipeline about the corpus and run it
			controller.setCorpus(corpus);
			controller.execute();
	    } catch (Exception e) {e.printStackTrace();}

		doc = (Document) corpus.iterator().next();

		AnnotationSet defaultAnnotSet = doc.getAnnotations();
		Set<String> annotTypesRequired = new HashSet<String>();
		annotTypesRequired.add("Person");
		annotTypesRequired.add("Location");
		annotTypesRequired.add("Organization");
		annotTypesRequired.add("Unknown");

		Set<Annotation> peopleAndPlaces = new HashSet<Annotation>(
				defaultAnnotSet.get(annotTypesRequired));

		FeatureMap features = doc.getFeatures();
		String originalContent = (String) features
				.get(GateConstants.ORIGINAL_DOCUMENT_CONTENT_FEATURE_NAME);
		RepositioningInfo info = (RepositioningInfo) features
				.get(GateConstants.DOCUMENT_REPOSITIONING_INFO_FEATURE_NAME);

		System.out.println(originalContent);
		Iterator<Annotation> it = peopleAndPlaces.iterator();
		Annotation currAnnot;
		SortedAnnotationList sortedAnnotations = new SortedAnnotationList();
		while (it.hasNext()) {
			currAnnot = (Annotation) it.next();
			sortedAnnotations.addSortedExclusive(currAnnot);
		}

		long insertPositionEnd;
		long insertPositionStart;
		// insert annotation tags backward
		LOGGER.info("Unsorted annotations count: " + peopleAndPlaces.size());
		LOGGER.info("Sorted annotations count: " + sortedAnnotations.size());

		List<TweetMention> mentions = new ArrayList<TweetMention>();
		for (int i = 0; i < sortedAnnotations.size(); ++i) {
			currAnnot = sortedAnnotations.get(i);
			insertPositionStart = currAnnot.getStartNode().getOffset().longValue();
			if (info != null)
				insertPositionStart = info.getOriginalPos(insertPositionStart);
			insertPositionEnd = currAnnot.getEndNode().getOffset().longValue();
			if (info != null)
				insertPositionEnd = info.getOriginalPos(insertPositionEnd, true);

			if (insertPositionEnd != -1 && insertPositionStart != -1) {
				String name = originalContent.substring((int)insertPositionStart, (int)insertPositionEnd);
				LOGGER.info(name + "[" + insertPositionStart + "-" + 
					insertPositionEnd + "]:" + currAnnot.getType());
				
				TweetMention mention = new TweetMention(name, (int)insertPositionStart, (int)insertPositionEnd, currAnnot.getType());
				mentions.add(mention);
			}
		}
		
		for (TweetMention tm : mentions)
			System.out.println(tm.name + "\t" + tm.start + "\t" + tm.end + "\t" + tm.type);
		return mentions;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static class TweetMention {
		public String name = null;
		public int start = -1;
		public int end = -1;
		public String type;
		public String entity = null;
		
		public TweetMention(String name, int start, int end, String type) {
			this.name = name;
			this.start = start;
			this.end = end;
			this.type = type;
		}

		public TweetMention(String name, int start, int end, String type, String entity) {
			this.name = name;
			this.start = start;
			this.end = end;
			this.type = type;
			this.entity = entity;
		}
	}
	
	public static class SortedAnnotationList extends Vector<Annotation> {
		private static final long serialVersionUID = 1L;

		public SortedAnnotationList() {
			super();
		}

		public boolean addSortedExclusive(Annotation annot) {
			Annotation currAnot = null;

			// overlapping check
			for (int i = 0; i < size(); ++i) {
				currAnot = (Annotation) get(i);
				if (annot.overlaps(currAnot))
					return false;
			}

			long annotStart = annot.getStartNode().getOffset().longValue();
			long currStart;
			// insert
			for (int i = 0; i < size(); ++i) {
				currAnot = (Annotation) get(i);
				currStart = currAnot.getStartNode().getOffset().longValue();
				if (annotStart < currStart) {
					insertElementAt(annot, i);
					return true;
				}
			}

			insertElementAt(annot, size());
			return true;
		}
	}


}
