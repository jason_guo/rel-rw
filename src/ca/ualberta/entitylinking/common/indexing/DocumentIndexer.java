package ca.ualberta.entitylinking.common.indexing;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.lang.Runnable;

import org.wikipedia.miner.annotation.Disambiguator;
import org.wikipedia.miner.model.Wikipedia;
import org.wikipedia.miner.model.Page;
import org.wikipedia.miner.util.WikipediaConfiguration;
import org.wikipedia.miner.util.PageIterator;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermFreqVector;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.search.DefaultSimilarity;
import org.apache.lucene.search.FieldCache;

/**
 * This class is mainly used to create an index for a document using 
 * a specificly defined tokenizer (Stanford Tokenizer).
 * 
 * @author zhaochen
 *
 */
public class DocumentIndexer {
	public static String wikiConfigFile = 
			"/nfs/data1/zhaochen/wikipedia-miner-1.2.0/configs/wikipedia-20130604.xml";
	
	private Wikipedia wikipedia = null;
	private Disambiguator disambiguator = null;

	public static class Input {
		public Input(String id, String content) {
			this.id = id;
			this.content = content;
		}
		
		public String id;
		public String content;
	}
	
	public static class Output {
		public Output(String id, List<Tokenizer.Token> tokens) {
			this.id = id;
			this.tokens = tokens;
		}
		
		public String id;
		public List<Tokenizer.Token> tokens;
	}
	
	public static class IndexThread implements Runnable {
		BlockingQueue<Output> outputQueue;
		DocumentIndexer indexer;

		public IndexThread(DocumentIndexer indexer, BlockingQueue<Output> outputQueue) {
			this.outputQueue = outputQueue;
			this.indexer = indexer;
		}
		
		public void run() {
			int count = 0;
			while (true) {
				if (count++ % 1000 == 0)
					System.out.println("Indexing pages: " + count);
				
				try {
					Output item = outputQueue.take();
					if (item.id == null && item.tokens == null)
						break;

					indexer.indexing(item.id, item.tokens);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			System.out.println("Done with indexing!!!");
		}
	}
	
	public DocumentIndexer() {
		this(wikiConfigFile);
	}
	
	public DocumentIndexer(String wikiConfigFile) {
		//Configure the Wikipedia Miner.
		try {
			File confFile = new File(wikiConfigFile);
			WikipediaConfiguration conf = new WikipediaConfiguration(confFile);
			wikipedia = new Wikipedia(conf, false);
			disambiguator = new Disambiguator(wikipedia);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** the following functions are mainly for testing.
	 */
	public static String getPlainText(String content) {
		String text = content.replaceAll("&gt;", ">");
		text = text.replaceAll("&lt;", "<");
		text = text.replaceAll("(?s)<ref.*?</ref>", " ");
		text = text.replaceAll("</?.*?>", " ");
		text = text.replaceAll("(?s)\\{\\{.*?\\}\\}", " ");
		text = text.replaceAll("\\[\\[.*?:.*?\\]\\]", " ");
//		text = text.replaceAll("\\[\\[(\\w+)\\|(\\w+)\\]\\]", "$2");
		text = text.replaceAll("\\[\\[(.*?)\\]\\]", "$1");
		text = text.replaceAll("(?s)\\<\\!\\-\\-(.*?)\\-\\-\\>", " ");
		text = text.replaceAll("\\[.*?\\]", " ");
		text = text.replaceAll("\\'+", "");
		text = text.replaceAll("\\|", " ");
		return text;
	}

	public void testPageIter() {
		int count = 0;
		PageIterator pageIter = wikipedia.getPageIterator(Page.PageType.article);
		while (pageIter.hasNext()) {
			count++;
			if (count % 1000 == 0)
				System.out.println("Reading pages: " + count);
			Page page = pageIter.next();
			String title = page.getTitle();
			String content = page.getMarkup();
			System.out.println(title);
		}
	}
	
	public void indexWikipedia(String indexDir) {
		initWriter(indexDir, true);
		
		int count = 0;
		
		//index.
		BlockingQueue<Input> inputQueue = new ArrayBlockingQueue<Input>(10);
		BlockingQueue<Output> outputQueue = new ArrayBlockingQueue<Output>(10);

		int THREAD_NUM = 16;
		Thread[] threads = new Thread[THREAD_NUM];
		for (int i = 0; i < threads.length; i++) {
			Tokenizer toker = new Tokenizer(wikipedia, disambiguator, inputQueue, outputQueue);
			threads[i] = new Thread(toker);
			threads[i].start();
		}
		
		IndexThread indexThread = new IndexThread(this, outputQueue);
		Thread index = new Thread(indexThread);
		index.start();

		PageIterator pageIter = wikipedia.getPageIterator(Page.PageType.article);
		while (pageIter.hasNext()) {
			count++;
			if (count % 1000 == 0)
				System.out.println("Reading pages: " + count);
			Page page = pageIter.next();
			String title = page.getTitle();
			String content = page.getMarkup();
			if (title == null || title.isEmpty() || content == null || content.isEmpty())
				continue;

			content = getPlainText(content);
			
			try {
				System.out.println(count + "\t" + title);
				inputQueue.put(new Input(title, content));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		//at the end of indexing, we put an exit message in the queue.
		try {
			for (int i = 0; i < THREAD_NUM; i++)
				inputQueue.put(new Input(null, null));

			for (int i = 0; i < THREAD_NUM; i++)
				threads[i].join();
		} catch (Exception e) {
			e.printStackTrace();
		}

		pageIter.close();
		
		try {
			outputQueue.put(new Output(null, null));
			index.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("DONE!!!!!!!1");
		
		//finalize.
		finalize();
	}
	
	IndexWriter writer = null;
	public void initWriter(String indexDir, boolean create) {
		try {
			Directory dir = FSDirectory.open(new File(indexDir));
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_34, null);

			// create a new index
			if (create)
				iwc.setOpenMode(OpenMode.CREATE);
			else
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);

			writer = new IndexWriter(dir, iwc);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void indexing(String name, List<Tokenizer.Token> tokens) {
		if (name == null || name.isEmpty() || tokens == null || tokens.isEmpty())
			return;
		
		try {
			// index a document.
			Document doc = new Document();
			
			doc.add(new Field("name", name, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
			
			for (Tokenizer.Token token : tokens) {
				doc.add(new Field("contents", token.text.toLowerCase(), Field.Store.YES,
						Field.Index.NOT_ANALYZED_NO_NORMS, Field.TermVector.YES));
			}
			
			if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
				writer.addDocument(doc);
			} else {
				writer.updateDocument(new Term("name", name), doc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void finalize() {
		try {
			writer.optimize();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void readLuceneIndex(String indexDir, String docName) {
		IndexReader reader = null;
		Map<String, Integer> name2id = null;
		
		//load index
		try {
			reader = IndexReader.open(FSDirectory.open(new File(indexDir)));

			String[] stringArray = FieldCache.DEFAULT.getStrings(reader, "name");

			// build a map from string to its document id.
			name2id = new HashMap<String, Integer>();
			for (int i = 0; i < stringArray.length; i++)
				name2id.put(stringArray[i], i);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//get tf-idf vector of a document.
		DefaultSimilarity simObj = new DefaultSimilarity();

		try {
			if (!name2id.containsKey(docName))
				return;
			
			int docId = name2id.get(docName);
			Document doc = reader.document(docId);
			
			TermFreqVector termVector = reader.getTermFreqVector(docId, "contents");
			int numDocs = reader.numDocs();

			int[] termFreq = termVector.getTermFrequencies();
			String[] terms = termVector.getTerms();
			for (int i = 0; i < terms.length; i++) {
				//avoid stop words
//				if (isStopWord(terms[i]))
//					continue;
				
				int tf = termFreq[i];
				int df = reader.docFreq(new Term("contents", terms[i]));
				float tfidf = simObj.tf(tf) * simObj.idf(df, numDocs);
				System.out.println(terms[i] + ": " + tfidf);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public static void main(String[] args) {
		DocumentIndexer indexer = new DocumentIndexer();
//		String test = "University of Alberta is $$ located in Edmonton, Alberta. It is a great university.";
//		System.out.println(test);
//		List<Token> tokenList = indexer.tokenize(test);
//		List<Token> labelList = indexer.extractWikiLabel(test);
//		indexer.mergeTokenList(tokenList, labelList);
//		String docId = "ua";
//		indexer.testIndexing("xxidx", docId, test);
		indexer.indexWikipedia(args[0]);
//		indexer.testPageIter();
//		String text = "xx\*hello word */";
//		text = text.replaceAll("(?s)\\\*.*?\\*\", " ");
//		DocumentIndexer indexer = new DocumentIndexer();
//		indexer.readLuceneIndex(args[0], args[1]);
		
	}
}

