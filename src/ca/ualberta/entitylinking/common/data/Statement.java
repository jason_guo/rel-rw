package ca.ualberta.entitylinking.common.data;

import java.io.Serializable;

/**
 * Statement
 * 
 * is a triple <Argument, Relation, Argument>.
 * Argument can be an Entity, Entity Mention or a Statement. 
 *  
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class Statement implements Argument, Serializable, Cloneable{

	private static final long serialVersionUID = 1L;
	
	protected Argument arg1;
	protected Argument arg2;
	protected Relation relation;
	protected Chunk relationChunk;
	protected double confidenceScore;
	
	public Statement(Argument arg1, Relation relation, Argument arg2){
		this.arg1 = arg1;
		this.relation = relation;
		this.arg2 = arg2;
	}

	@Override
	public Statement clone() throws CloneNotSupportedException{

		Argument newArg1 = arg1.clone();
		Argument newArg2 = arg2.clone();
		Relation newRelation = relation.clone();
		
		Statement newStatement = new Statement(newArg1, newRelation, newArg2);
		
		return newStatement;
	}

	public Chunk getRelationChunk() {
		return relationChunk;
	}

	public void setRelationChunk(Chunk relationChunk) {
		this.relationChunk = relationChunk;
	}

	public Argument getArg1() {
		return arg1;
	}

	public void setArg1(Argument arg1) {
		this.arg1 = arg1;
	}

	public Argument getArg2() {
		return arg2;
	}

	public void setArg2(Argument arg2) {
		this.arg2 = arg2;
	}

	public Relation getRelation() {
		return relation;
	}

	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	public double getConfidenceScore() {
		return confidenceScore;
	}

	public void setConfidenceScore(double confidenceScore) {
		this.confidenceScore = confidenceScore;
	}
		
}
