package ca.ualberta.entitylinking.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Relation
 * 
 * contains information about a relation. 
 * It is used to output relations from the clustering phase and refine relations in the refinement phase.  
 * 
 *  
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class Relation  implements Serializable, Comparable<Relation>, Cloneable{

	private static final long serialVersionUID = 1;

	/** Relation id. */
	protected String id;
	
	/** The relation label, a meaningful name to this relation. */
	protected String label;

	/** A list of possible labels ordered from the most to the least meaningful label. */
	protected Map<String, Double> candidateLabels;

	/** The entity tuples belonging to this relation mapped to the number of times each tuple was extracted. */
	protected Map<Tuple,Integer> tuples;
	
	public Map<String, Double> getCandidateLabels() {
		return candidateLabels;
	}

	/** Creates a relation with no id. */
	public Relation(){
		this(null,new LinkedHashMap<Tuple,Integer>());
	}

	/** Creates a relation with id and an empty list of tuples. */
	public Relation(String id){
		this(id, new LinkedHashMap<Tuple,Integer>());
	}
	
	/** Creates a relation with id, label, and an empty list of tuples. */
	public Relation(String id, String label){
		this(id, label, new LinkedHashMap<Tuple,Integer>());
	}
	
	/** Creates a relation with id and tuples. */
	public Relation(String id, Map<Tuple,Integer> tuples){
		this(id, null, tuples);
	}

	/** Creates a relation with id, label and tuples. */
	public Relation(String id, String label, Map<Tuple,Integer> tuples){
		this.id = id;
		this.label = label;
		this.tuples = tuples;
		candidateLabels = new LinkedHashMap<String,Double>();
	}
	
	/** Clone this object */
	public Relation clone() throws CloneNotSupportedException{
		Relation newRelation = new Relation(id, label);
		
		for(Entry<String,Double> entry : candidateLabels.entrySet()){
			newRelation.addCandidateLabel(entry.getKey(), entry.getValue());
		}
		
		for(Entry<Tuple,Integer> entry : tuples.entrySet()){
			newRelation.addTuple(entry.getKey().clone(), entry.getValue());
		}
		
		return newRelation;
	}

	/** Add a tuple to this relation. */
	public void addTuple(Tuple tuple) {
		if(tuple == null) return;
		
		int multiplicity = 1;
		if(tuples.containsKey(tuple)){
			multiplicity = tuples.get(tuple) + 1;
		}
		addTuple(tuple, multiplicity);
	}

	/** Add a tuple to this relation with a pre-defined multiplicity */
	public void addTuple(Tuple tuple, int multiplicity) {
		if(tuple == null) return;

		tuples.put(tuple, new Integer(multiplicity));
	}
	
	public void removeTuple(Tuple tuple){
		tuples.remove(tuple);
	}

	public Map<Tuple,Integer> getTuples() {
		return tuples;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public List<String> getTopCandidateLabels(int k) {
		
		List<Entry<String,Double>> ranking = getTopCandidateLabelsWithWeight(k);
		List<String> myRanking = new ArrayList<String>(k);
		for(Entry<String,Double> entry : ranking){
			myRanking.add(entry.getKey());
		}
		return myRanking;
	}
	
	public List<Entry<String,Double>> getTopCandidateLabelsWithWeight(int k) {
		
		// Sort features by weight
		List<Entry<String,Double>> list = new LinkedList<Entry<String,Double>>(candidateLabels.entrySet());
		Collections.sort(list, new Comparator<Entry<String,Double>>() {
			// descending order
			public int compare(Entry<String,Double> o1, Entry<String,Double> o2) {
				return (-1 * o1.getValue().compareTo(o2.getValue()));
			}
		});

		List<Entry<String,Double>> labelRanking = new ArrayList<Entry<String,Double>>(k);
		
		int count=0;
		for (Iterator<Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			count++;
			if(count > k){
				break;
			}
			Entry<String, Double> entry = it.next();
			labelRanking.add(entry);
		}
		
		return labelRanking;
	}

	public void addCandidateLabel(String label, Double weight){
		candidateLabels.put(label, weight);
	}
	
	@Override
	public String toString() {
		return this.id + " : " + this.tuples;
	}
	
	public String detailedToString() {
		return this.id + "\t" + this.label;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode() + this.tuples.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Relation that = (Relation)obj;
		return this.id.equals(that.id) && this.tuples.equals(that.tuples);
	}

	@Override
	public int compareTo(Relation o) {
		return this.id.compareTo(o.id);
	}

	private void mergeWith(Relation relationToMerge) {
		for (Tuple t : relationToMerge.getTuples().keySet()) {
			this.addTuple(t, relationToMerge.getTuples().get(t));
		}
	}

//	public static void writeRelations(Iterable<Relation> relations, Writer out) {
//		try {
//			for (Relation relation : relations) {
//				out.append("R\t" + relation.getLabel() + "\n");
//				for (Tuple t : relation.getTuples().keySet()) {
//					out.append("T\t" + t + "\n");
//				}
//				out.append("\n");
//			}
//		} catch (IOException e) {
//			throw new IllegalStateException(e);
//		}
//	}
//	
//	public static List<Relation> readRelations(Reader fileReader) {
//		try {
//			List<Relation> result = new ArrayList<Relation>();
//			BufferedReader in = new BufferedReader(fileReader);
//			String line;
//			Relation actual = null;
//			while ((line = in.readLine()) != null) {
//				if (line.isEmpty()) {
//					continue;
//				}
//				String[] splitted = line.split("\t");
//				if ("R".equals(splitted[0])) {
//					if (actual != null) {
//						result.add(actual);
//					}
//					actual = new Relation(splitted[1]);
//				}
//				if ("T".equals(splitted[0])) {
//					String[] e1 = splitted[1].split("#");
//					String[] e2 = splitted[2].split("#");
//					actual.addTuple(new Tuple(Arrays.asList(
//							new Entity("no_id", e1[1],e1[0]),
//							new Entity("no_id", e2[1],e2[0]))));
//				}
//			}
//			in.close();
//			return result;
//		} catch (IOException e) {
//			throw new IllegalStateException(e);
//		}
//	}

	public static Set<Tuple> getAllTuples(List<Relation> groundTruthRelations) {
		Set<Tuple> relationTuples = new LinkedHashSet<Tuple>();
		for(Relation groundTruthRelation : groundTruthRelations){
			relationTuples.addAll(groundTruthRelation.getTuples().keySet());
		}
		return relationTuples;
	}

	public static List<Relation> mergeRelations(
			List<Relation> relations,
			List<Relation> toMerge) {
		Map<String,Relation> result = new LinkedHashMap<String, Relation>();
		for (Relation relation: relations) {
			result.put(relation.getId(), relation);
		}
		for (Relation relationToMerge: toMerge) {
			String actualLabel = relationToMerge.getId();
			Relation candidateToMerge = result.get(actualLabel);
			if (candidateToMerge != null) {
				candidateToMerge.mergeWith(relationToMerge);
				//				result.put(actualLabel, candidateToMerge.mergeWith(relationToMerge));
			} else {
				result.put(actualLabel, relationToMerge);
			}
		}
		return new ArrayList<Relation>(result.values());
	}

}
