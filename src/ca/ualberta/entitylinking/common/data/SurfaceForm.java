package ca.ualberta.entitylinking.common.data;

/**
 * Created by zhaochen on 26/10/16.
 */
public class SurfaceForm {
    public String text;
    public int offset;
    public int length;

    public SurfaceForm(String text, int offset, int length) {
        this.text = text;
        this.offset = offset;
        this.length = length;
    }
}
