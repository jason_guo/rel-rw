package ca.ualberta.entitylinking.common.data;

/**
 *	Document
 *
 * stores information about a document (e.g., news article, blog post)
 * 
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 * 
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Document implements Serializable{
	
	private static final long serialVersionUID = 1; 

	private String id; 
	private String fileName;
	
	/* metadata */
	private String url;
	private String title;
	private Date publishedDate;

	/* document contents */
	private transient String originalText; //original text
	private transient String asciiText; //ascii text

	private List<Sentence> sentences;
	
	public Document() {
		sentences = new ArrayList<Sentence>();
	}
	
	public Document(String id, String url, String title, String originalText){

		this.id = id;
		this.url = url;
		this.title = title;
		this.originalText = originalText;

		sentences = new ArrayList<Sentence>();

	}
	
	public String toPlainTextBracketFormat(){
		StringBuilder builder = new StringBuilder(10000);
		builder.append("D\tfile=");
		if(this.getFileName() != null) builder.append(this.getFileName());
		builder.append("\tid=");
		if(this.getId() != null) builder.append(this.getId());
		builder.append("\ttitle=");
		if(this.getTitle() != null) builder.append(this.getTitle());
		builder.append("\tdate=");
		if( this.getPublishedDate() != null) builder.append(this.getPublishedDate());
		builder.append("\turl=");
		if(this.getUrl() != null) builder.append(this.getUrl());
		builder.append("\n");
		
		for(Sentence sentence : this.getSentences()){
			builder.append(sentence.toPlainTextBracketFormat());
			builder.append("\n");
		}
		return builder.toString();
	}
	
	public void addSentence(Sentence sentence){
		int number = sentences.size();
		sentences.add(sentence);
		sentence.setNumber(number);
		sentence.setDocument(this);
	}

	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append(title + " (" + url + ")\n");
		for(Sentence sentence : sentences){
			buffer.append(sentence.toString());
		}
		return buffer.toString();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
//		System.out.println("[File Name]: " + this.fileName);
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getOriginalText() {
		return originalText;
	}

	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}

	public String getAsciiText() {
		return asciiText;
	}

	public void setAsciiText(String asciiText) {
		this.asciiText = asciiText;
	}

	public List<Sentence> getSentences() {
		return sentences;
	}

	public void setSentences(List<Sentence> sentences) {
		this.sentences = sentences;
	}
	
}
