package ca.ualberta.entitylinking.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Sentence
 * 
 * contains information about a sentence. 
 * Sentences are represented as a list of tokens.
 * Each token can be annotated with entities, part-of-speech tags and relations, for example.
 * 
 *  
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class Sentence implements Serializable, Cloneable{

	private static final long serialVersionUID = 1;

	/** The document containing this sentence */
	protected Document document;

	/** The sentence number in the document. Starts at zero. 
	 * Must match the index in {@link Document.getSentences()}. */
	protected int number;

	/** The tokens in a sentence */
	protected List<Token> tokens;

	/** Entity mentions*/
	protected LinkedList<Mention> mentions;

	/** Triples */
	protected LinkedList<Statement> statements;

	/** Constructor of empty sentences (containing no tokens). */
	public Sentence(){
		this(new ArrayList<Token>());
	}

	/** Complete constructor. 
	 * @param tokens A list of tokens.
	 * */
	public Sentence(List<Token> tokens){
		this.tokens = tokens;
		mentions = new LinkedList<Mention>();
		statements = new LinkedList<Statement>();
	}

	@Override
	public Sentence clone() throws CloneNotSupportedException{

		Sentence newSentence = new Sentence();

		for(Token token : this.getTokens()){
			tokens.add((Token) token.clone());
		}

		if(this.getMentions() != null){
			for(Mention mention : this.getMentions()){
				Mention newMention = (Mention) mention.clone();
				newMention.setSentence(newSentence);
				mentions.add(newMention);
			}
		}

		if(this.getStatements() != null){
			for(Statement statement : this.getStatements()){
				Statement newStatement = (Statement) statement.clone();
				statements.add(newStatement);
			}
		}

		return newSentence;

	}

	/**
	 * toPlainTextSquareBracketFormat
	 * 
	 * @return this sentence in plain text, where mentions are annotated with square brackets and relations with curly brackets. 
	 * For example: "[[[PER Barack Obama]]] was {{{seen in}}} the [[[LOC White House]]] ."
	 */
	public String toPlainTextBracketFormat(){

		Map<Integer, Mention> startMention = new HashMap<Integer, Mention>();
		Map<Integer, Mention> endMention= new HashMap<Integer, Mention>();
		Map<Integer, Statement> startRelation = new HashMap<Integer, Statement>();
		Map<Integer, Statement> endRelation= new HashMap<Integer, Statement>();

		if(this.getMentions() != null){
			for(Mention mention : this.getMentions()){
				startMention.put(mention.getStartToken(), mention);
				endMention.put(mention.getEndToken(), mention);
			}
		}

		if(this.getStatements() != null){
			for(Statement statement : this.getStatements()){
				startRelation.put(statement.getRelationChunk().getStartToken(), statement);
				endRelation.put(statement.getRelationChunk().getEndToken(), statement);
			}
		}

		StringBuffer sentence = new StringBuffer();

		for(int i=0; i< this.getTokens().size(); i++){
			Token token = this.getTokens().get(i);

			if(startMention.containsKey(i)){
				sentence.append("[[[" + startMention.get(i).getEntity().getType() + " ");
			}

			if(startRelation.containsKey(i)){
				sentence.append("{{{");
			}

			sentence.append(token.getText());

			if(endMention.containsKey(i)){
				sentence.append("]]]");
			}

			if(endRelation.containsKey(i)){
				sentence.append("}}}");
			}

			sentence.append(" ");

		}

		return sentence.toString().trim();
	}

	public String getAnnotatedText(){
		StringBuffer buffer = new StringBuffer();

		Map<Integer, Mention> startMention = new HashMap<Integer, Mention>();
		Map<Integer, Mention> endMention = new HashMap<Integer, Mention>();
		Map<Integer, Statement> startRelation = new HashMap<Integer, Statement>();
		Map<Integer, Statement> endRelation= new HashMap<Integer, Statement>();

		if(this.getMentions() != null){
			for(Mention mention : this.getMentions()){
				startMention.put(mention.getStartToken(), mention);
				endMention.put(mention.getEndToken(), mention);
			}
		}
		
		if(this.getStatements() != null){
			for(Statement statement : this.getStatements()){
				startRelation.put(statement.getRelationChunk().getStartToken(), statement);
				endRelation.put(statement.getRelationChunk().getEndToken(), statement);
			}
		}

		for(int i=0; i < tokens.size(); i++){

			Token token = tokens.get(i);

			if(startMention.containsKey(i))
				buffer.append("<entity type=\"" + startMention.get(i).getEntity().getType() + "\" id=\""+ startMention.get(i).getEntity().getId() + "\">");
			
			if(startRelation.containsKey(i)){
				buffer.append("<relation>");
			}

			if(token.getAnnotations().get(Token.POS_ANNOTATION) == null)
				buffer.append(token.getText());
			else
				buffer.append(token.getText() + "/" + token.getAnnotations().get(Token.POS_ANNOTATION));

			if(endMention.containsKey(i))
				buffer.append("</entity>");
			
			if(endRelation.containsKey(i)){
				buffer.append("</relation>");
			}

			if(i != tokens.size()-1)
				buffer.append(" ");
		}

		return buffer.toString();
	}

	/**
	 * Returns the list of mentions in this sentence.
	 * @return mentions List of mentions.
	 */
	public List<Mention> getMentions() {
		return mentions;
	}

	/**
	 * Adds a mention into this sentence (and sets the sentence attribute of the mention).
	 * The mention is inserted as to keep the mention list in order of startToken (first) and endToken (second). 
	 * @param mention A mention object.
	 */
	public void addMention(Mention mention) {
		mention.setSentence(this);
		// Empty list
		if(mentions.size()==0){
			mentions.add(mention);
			return;
		}

		// Add in the end
		if(mention.compareTo(mentions.getLast())>0){
			mentions.addLast(mention);
			return;
		}

		// Add mention in the middle of the list
		for(int i=0;i<mentions.size();i++){
			if(mention.compareTo(mentions.get(i)) < 0){
				mentions.add(i, mention);
				return;
			}

			// In case of tie, don't add this mention since it already exists.
			if(mention.compareTo(mentions.get(i))==0)
				return;

		}

	}

	public List<Statement> getStatements(){
		return statements;
	}

	public void addStatement(Statement newStatement){
		statements.add(newStatement);
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getText(){
		StringBuffer buffer = new StringBuffer(100);

		for(Token token : getTokens()){
			buffer.append(" " + token);
		}

		return buffer.toString().trim();
	}


	public String toString() {
		StringBuffer buffer = new StringBuffer(100);

		buffer.append("Sentence: ");
		buffer.append(this.getText());
		buffer.append("\n");

		buffer.append("Mentions:");
		for(Mention mention : getMentions()){
			buffer.append(" [" + mention + "]");
		}
		buffer.append("\n");


		return buffer.toString();
	}

	public List<Token> getTokens() {
		return tokens;
	}

	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
