package ca.ualberta.entitylinking.common.data;

public interface Argument extends Cloneable{

	public Argument clone() throws CloneNotSupportedException;
	
}
