package ca.ualberta.entitylinking.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Tuple
 * 
 * contains information about a tuple. A tuple is a list of entities that belong to a relation. 
 * 
 *  
 * @author Filipe Mesquita <mesquita@cs.ualberta.ca>
 *
 */

public class Tuple implements Serializable, Cloneable{
	
	private static final long serialVersionUID = 1;
	
	/** Number of sentences containing this tuple. */
	protected int support;
	
	/** List of entities forming this tuple. */
	protected List<Entity> entities;
	
	/** Weight on this tuple in the relation it belongs to. */
	protected double relationWeight;
	
	/** Creates a tuple with an empty list of entities. */
	public Tuple (){
		this(new ArrayList<Entity>());
	}
	
	/** Creates a tuple with a given list of entities. */
	public Tuple (List<Entity> entities) {
		this.entities = entities;
	}

	public Tuple clone() throws CloneNotSupportedException{
		Tuple newTuple = new Tuple();
		newTuple.setSupport(support);
		newTuple.setRelationWeight(relationWeight);

		ArrayList<Entity> newEntities = new ArrayList<Entity>();
		for(Entity entity : entities){
			Entity newEntity = entity.clone();
			newEntities.add(newEntity);
		}
		
		newTuple.setEntities(newEntities);
		
		return newTuple;
		
	}
	
	public List<Entity> getEntities() {
		return entities;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	
	public int getSupport() {
		return support;
	}

	public void setSupport(int support) {
		this.support = support;
	}
	
	public double getRelationWeight() {
		return relationWeight;
	}

	public void setRelationWeight(double relationWeight) {
		this.relationWeight = relationWeight;
	}
	
	@Override
	public String toString() {
		return entities.toString();
	}
	
	@Override
	public int hashCode() {
		int result = 0;
		for (Entity e : this.entities) {
			result += e.hashCode();
		}
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		Tuple that = (Tuple)obj;
		return this.entities.equals(that.entities);
	}

}
