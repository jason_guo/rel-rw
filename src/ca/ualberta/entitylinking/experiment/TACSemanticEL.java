package ca.ualberta.entitylinking.experiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ca.ualberta.entitylinking.common.data.Document;
import ca.ualberta.entitylinking.common.data.Mention;
import ca.ualberta.entitylinking.common.data.Sentence;
import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;
import ca.ualberta.entitylinking.common.indexing.Tokenizer;
import ca.ualberta.entitylinking.common.nlp.OrthoMatcherCoref;
import ca.ualberta.entitylinking.common.nlp.StanfordNER;
import ca.ualberta.entitylinking.config.ELConfig;
import ca.ualberta.entitylinking.cs.CandidateSelection;
import ca.ualberta.entitylinking.cs.CandidateSelectionLuceneSimple;
import ca.ualberta.entitylinking.cs.MentionExpansion;
import ca.ualberta.entitylinking.disambiguation.L2RPredictor;
import ca.ualberta.entitylinking.graph.DirectedGraph;
import ca.ualberta.entitylinking.graph.GraphUtils;
import ca.ualberta.entitylinking.graph.SubGraphGenerator;
import ca.ualberta.entitylinking.graph.UndirectedGraph;
import ca.ualberta.entitylinking.graph.WeightedGraph;
import ca.ualberta.entitylinking.graph.algorithms.PersonalizedPageRank;
import ca.ualberta.entitylinking.graph.algorithms.UnweightedPersonalizedPageRank;
import ca.ualberta.entitylinking.graph.algorithms.WeightedPersonalizedPageRank;
import ca.ualberta.entitylinking.graph.similarity.context.EntityContextCache;
import ca.ualberta.entitylinking.graph.similarity.context.MentionContextCache;
import ca.ualberta.entitylinking.graph.similarity.measure.SimilarityMeasure;
import ca.ualberta.entitylinking.utils.CSUtils;
import ca.ualberta.entitylinking.utils.DocumentUtils;
import ca.ualberta.entitylinking.utils.ELUtils;
import ca.ualberta.entitylinking.utils.Feature;
import ca.ualberta.entitylinking.utils.Rank;
import ca.ualberta.entitylinking.utils.XmlProcessor;
import ca.ualberta.entitylinking.utils.similarity.StringSim;
import ca.ualberta.entitylinking.utils.similarity.VectorSimilarity;

public class TACSemanticEL {
	public final static String DOC_PATH="/nfs/data1/corpora/tac/tac11/source/TAC_2010_KBP_Source_Data/data/";
	private static Logger LOGGER = LogManager.getLogger(TACSemanticEL.class);
    public static DecimalFormat df = new DecimalFormat("#.###");

	HashSet<String> entitySet = null; 

	protected StanfordNER ner = null;
	protected OrthoMatcherCoref orthoMatcher = null;
	protected TFIDF3x tfidfIndex = null;
	protected Tokenizer toker = null;
	protected CandidateSelection cs = null;
	protected MentionExpansion me = null;

    private SubGraphGenerator gg = null;
    private WeightedGraph g = null;
    
    private L2RPredictor predictor = null;

	//cache the context of mentions for efficiency.
    MentionContextCache mentionCtxCache = null;
    //cache the context of entities for efficiency.
    EntityContextCache entityCtxCache = null;

    
    private class WeightCache {
        Map<Entity, Double> randomWeightCache = new HashMap<Entity, Double>();
        Map<Mention, Double> prefWeightCache = new HashMap<Mention, Double>();
        Map<Mention, Map<Entity, Double>> localSimMap =
                new HashMap<Mention, Map<Entity, Double>>();
        Map<Mention, Map<Entity, Double>> priorProbMap = null;

        public void prepareWeightCache(ELConfig.PrefStrategy pref,
                                       List<Mention> mentions,
                                       Map<Mention, Map<Entity, Double>> candMap) {
            if (pref == ELConfig.PrefStrategy.RANDOM) {
                Map<Entity, Double> candidates = null;
                randomWeightCache = new HashMap<Entity, Double>();
                Random rand = new Random(System.currentTimeMillis());
                for (Mention m : mentions) {
                    candidates = candMap.get(m);
                    if (candidates == null || candidates.isEmpty())
                        continue;

                    for (Entity e : candidates.keySet()) {
                        double weight = rand.nextDouble();
                        randomWeightCache.put(e, weight);
                    }
                }
            } else if (pref == ELConfig.PrefStrategy.TFIDF) {
                Document doc = mentions.get(0).getSentence().getDocument();
                String content = doc.getOriginalText();
                for (Mention m : mentions) {
                    String name = m.getName();
                    double tfidf = DocumentUtils.computeTFIDF(name, content, tfidfIndex);
                    prefWeightCache.put(m, tfidf);
                }
            }
        }

        public void prepareContextSimCache(List<Mention> mentions,
                                           Map<Mention, Map<Entity, Double>> candMap) {
            Map<Entity, Double> candidates = null;
            Map<Entity, Double> simMap = null;

            for (Mention m : mentions) {
                candidates = candMap.get(m);
                if (candidates == null || candidates.isEmpty())
                    continue;

                simMap = localSimMap.get(m);
                if (simMap == null)
                    simMap = new HashMap<Entity, Double>();

                if (candidates.size() == 1) {
                    simMap.put(candidates.keySet().iterator().next(), 1.0);
                } else {
                    for (Entity e : candidates.keySet()) {
                        //compute the context similarity with the candidate.
                        double local = SimilarityMeasure.mentionEntitySimilarity(m, e, mentionCtxCache, entityCtxCache);
                        simMap.put(e,  local);
                    }
                }

                localSimMap.put(m, simMap);
            }
        }

        public void preparePriorProbCache(List<Mention> mentions,
                                          Map<Mention, Map<Entity, Double>> candMap) {

            priorProbMap = candMap;
        }

        public double getMentionWeight(Mention m, ELConfig.PrefStrategy mPref) {
            if (mPref == ELConfig.PrefStrategy.UNIFORM)
                return 1.0;
            if (mPref == ELConfig.PrefStrategy.TFIDF)
                return prefWeightCache.get(m);

            return 1.0;
        }

        public double getEntityWeight(Mention m, Entity e, ELConfig.PrefStrategy ePref) {
            if (ePref == ELConfig.PrefStrategy.PRIOR_PROB)
                return priorProbMap.get(m).get(e);
            else if (ePref == ELConfig.PrefStrategy.CTX_SIM)
                return localSimMap.get(m).get(e);
            else if (ePref == ELConfig.PrefStrategy.RANDOM)
                return randomWeightCache.get(e);

            return 1.0;
        }
    }

    public TACSemanticEL(String configFile) {
		ELConfig.loadConfiguration(configFile);

        //Create the NER and co-reference resolution components.
		orthoMatcher = new OrthoMatcherCoref();
		Set<String> allowedEntityTypes = new HashSet<String>();
		allowedEntityTypes.add(Entity.PERSON);
		allowedEntityTypes.add(Entity.ORGANIZATION);
		allowedEntityTypes.add(Entity.LOCATION);
		allowedEntityTypes.add(Entity.MISC);
		ner = new StanfordNER(allowedEntityTypes);
        LOGGER.info(ELUtils.currentTime() + "Done with loading StanfordNER and GATE OrthoMatcher");

        //Load the Knowledge base graph.
		if (ELConfig.directedGraph)
			g = new DirectedGraph(ELConfig.linkGraphLoc);
		else
			g = new UndirectedGraph(ELConfig.cooccurrenceGraphLoc);
		
		g.load();
		gg = new SubGraphGenerator(g);
        LOGGER.info(ELUtils.currentTime() + "Done with loading graph");

        //Candidate selection
		cs = new CandidateSelectionLuceneSimple();
        LOGGER.info(ELUtils.currentTime() + "Done with loading lucene index");
		me = new MentionExpansion(ner, orthoMatcher, cs);

        LOGGER.info("Here: " + ELConfig.supervised);
        //load the prediction model.
        if (ELConfig.supervised)
        	predictor = new L2RPredictor(ELConfig.modelFile);

		try {
            tfidfIndex = new TFIDF3x();
            LOGGER.info(ELUtils.currentTime() + "Done with loading the TFIDF index");
			toker = new Tokenizer();
			
            mentionCtxCache = new MentionContextCache(ELConfig.contextOption, toker, tfidfIndex);
            entityCtxCache = new EntityContextCache(tfidfIndex);
        } catch (Exception e) {
			e.printStackTrace();
		}
    }
    
	public void loadKBEntities(String entityFile) {
		try {
			if (entitySet == null)	entitySet = new HashSet<String>();
			String line = null;
			BufferedReader r = new BufferedReader(new FileReader(entityFile));
			while ((line = r.readLine()) != null) {
				String toks[] = line.split("\t");
				entitySet.add(toks[0].trim());
			}
			
			r.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getPath(String docid) {
		String prefix1 = DOC_PATH + "/2009/nw";
		String prefix2 = DOC_PATH + "/2010/wb";
		String prefix3 = DOC_PATH + "/2009/wb";
		
		String path = null;
		if (docid.startsWith("eng")) {
			path = prefix2 + "/" + docid + ".sgm";
		} else if (docid.contains(".com_")) {
			path = prefix3 + "/" + docid + ".sgm";
		} else if (docid.startsWith("cmn")) {
			path = null;
		} else if (docid.contains("_CMN_20")) {
			path = null;
		} else {
			String toks1[] = docid.toLowerCase().split("_");
			String toks2[] = toks1[2].split("\\.");
		
			path = prefix1 + "/" + toks1[0] + "_" + toks1[1] + "/" + toks2[0] + "/" + docid + ".sgm";
		}
		
		if (path == null)
			return null;
		
		File f = new File(path);
		if (f.exists())
			return path;
		
		return null;
	}
	
	public static List<Mention> getMentions(Document doc) {
		List<Mention> mentions = new ArrayList<Mention>();
		for (Sentence sentence : doc.getSentences()) {
			for (Mention mention : sentence.getMentions()) {
				mentions.add(mention);
			}
		}
		
		return mentions;
	}

	/**
	 * Check if the given mention exists in the list of mentions.
	 * If yes - replace the mention found with the given mention m.
	 * Otherwise - add the new mention into the list.
	 * 
	 * @param m
	 * @param mentions
	 * @return
	 */
	private static Mention matchMention(Mention mention, List<Mention> mentions) {
		if (mention == null)	return null;
		String name = mention.getName();

		//find exact match.
		for (int i = 0; i < mentions.size(); i++) {
			Mention m = mentions.get(i);
			String mName = m.getName();
			if (mName.equalsIgnoreCase(name)) {
				mention.setEntity(m.getEntity());
				mentions.set(i, mention);
				return mention;
			}
		}

		//find mentions with partial matches - only do this when the given mention cannot be found.
		for (int i = 0; i < mentions.size(); i++) {
			Mention m = mentions.get(i);
			String mName = m.getName();
			if (mName.contains(name)) {
				mention.setEntity(m.getEntity());
				mentions.set(i, mention);
				return mention;
			}
		}

		//Add the given mention to the list if we cannot find the given mention.
		mentions.add(mention);
		
		return mention;
	}
	
    public Map<Entity, Double> selectCandidatesPruning(
    		Mention mention, Document doc, List<Mention> mentions, 
            MentionContextCache menCtxCache,
            EntityContextCache entCtxCache,
            CandidateSelection myCs) {

        if (mention == null)
            return null;

        Map<Entity, Double> candidates = null, temp1 = null, temp2 = null;
		candidates = me.selectCandidateMentionExpansion(mention, doc, mentions);

        if (candidates == null || candidates.isEmpty()) {
            LOGGER.info("\t" + mention.getName() + " : No candidates!");
            return null;
        }

        int K = 30;
        temp1 = CSUtils.pruneCandidatesWithContext(menCtxCache.getContext(mention), entCtxCache, candidates, K);
        temp2 = CSUtils.pruneCandidatesWithPriorProb(candidates, K);

        temp1.putAll(temp2);
        candidates.clear();
        double priorThreshold = 0.00002;
        for (Entity ent : temp1.keySet()) {
            double prior = temp1.get(ent);
            //prune candidates with prior probability < 0.00
            if (prior < priorThreshold) {
            	LOGGER.info(ent.getName() + "[prior=" + prior + "] is pruned");
                continue;
            }

            candidates.put(ent, prior);
        }

        return candidates;
    }
	
	Map<Mention, Map<Entity, Double>> selectCandidatesMention(Mention target, Document doc,
			List<Mention> mentions,
            MentionContextCache menCtxCache,
            EntityContextCache entCtxCache ) {
		
		Map<Mention, Map<Entity, Double>> ret = new HashMap<Mention, Map<Entity, Double>>();
		Map<Entity, Double> map = null;
		
		if (mentions == null || mentions.isEmpty())
			return null;
		
		for (Mention m : mentions) {
			if (m == target) {
				map = this.selectCandidatesPruning(m, doc, mentions, menCtxCache, entCtxCache, cs);
//				map = me.selectCandidateMentionExpansion(m, doc, mentions);
			} else {
				map = CSUtils.selectCandidatesPruning(m, menCtxCache, entCtxCache, cs);
			}
			
			if (map == null || map.isEmpty()) {
				LOGGER.info("\t" + m.getName() + " : No candidates!");
				continue;
			}
			
			ret.put(m, map);
		}
		
		return ret;
	}
	
    /**
     * Collect the entities of unambiguous mentions.
     *
     * @param candMap
     * @return The list of entities of unambiguous mentions.
     */
    private Map<String, Double> getUnambiguousEntities(
            Map<Mention, Map<Entity, Double>> candMap, WeightCache weightCache) {

        Map<String, Double> ret = new HashMap<String, Double>();
        Map<Entity, Double> candidates = null;

        for (Mention m : candMap.keySet()) {
            LOGGER.info(m.getEntity().getName());

            candidates = candMap.get(m);
            if (candidates == null || candidates.isEmpty())
                continue;

            //get the weight
            double mWeight = weightCache.getMentionWeight(m, ELConfig.mPrefStreg);
            if (candidates.size() == 1) {
                LOGGER.info("\t1: " + candidates.keySet().iterator().next().getName());
                ret.put(candidates.keySet().iterator().next().getName(), mWeight);
                continue;
            }

            //we select entities with both maximum prior probability
            //and local compatibility with mentions.
            double prior, sim, maxPrior = 0.0, maxSim = 0.0;
            Entity maxPriorEnt = null, maxSimEnt = null;
            for (Entity ent : candidates.keySet()) {
                prior = candidates.get(ent);
                sim = SimilarityMeasure.mentionEntitySimilarity(m, ent, mentionCtxCache, entityCtxCache);

                if (prior > maxPrior) {
                    maxPrior = prior;
                    maxPriorEnt = ent;
                }
                if (sim > maxSim) {
                    maxSim = sim;
                    maxSimEnt = ent;
                }
            }

            if (maxPriorEnt == maxSimEnt) {
                ret.put(maxPriorEnt.getName(), mWeight);
                LOGGER.info("\t2: " + maxPriorEnt.getName());

                // remove the rest candidates.
                prior = candidates.get(maxSimEnt);
                candidates.clear();
                candidates.put(maxSimEnt, prior);
            }
        }

        return ret;
    }

	/**
	 * Collect all the entities in the candidates of mentions as the nodes of the graph,
	 * Also we could expand the entity set with incoming links and outgoing links.
	 * 
	 * @param candMap
	 * @return
	 */
	private Set<String> collectNodes(Set<Mention> mentions,
			Map<Mention, Map<Entity, Double>> candMap) {
		
		if (candMap == null || candMap.isEmpty())
			return null;
		
		Set<String> ret = new HashSet<String>();
		Map<Entity, Double> candidates = null;
		for (Mention m : mentions) {
			candidates = candMap.get(m);
			if (candidates == null || candidates.isEmpty())
				continue;
			
			for (Entity e : candidates.keySet()) {
				String entName = e.getName();
				// check if the entName is in the Wikipedia Graph.
				if (!g.containsNode(entName))
					continue;
				
				ret.add(entName);
			}
		}
		
		return ret;
	}

    /**
     * Clear the candidates by removing entities not in the graph.
     * @param candidates
     * @param entities
     * @param e2id
     * @param ranks
     */
    private void cleanupCandidates(Map<Entity, Double> candidates, Set<String> entities,
                                   Map<String, Integer> e2id,
                                   Map<Integer, List<Double>> ranks) {

        if (candidates == null || candidates.size() <= 1)
            return;

        Entity[] candEntities = candidates.keySet().toArray(new Entity[1]);
        for (int j = 0; j < candEntities.length; j++) {
            Entity ent = candEntities[j];
            String entName = ent.getName();
            if (!entities.contains(entName) ||
                    !e2id.containsKey(entName) ||
                    !ranks.containsKey(e2id.get(entName)))

                candidates.remove(ent);
        }
    }
    
	private Map<String, Double> getApproximateEntities(
			Map<Mention, Map<Entity, Double>> candMap, WeightCache weightCache) {

		Map<String, Double> ret = new HashMap<String, Double>();
		Map<Entity, Double> candidates = null;

		for (Mention m : candMap.keySet()) {
			candidates = candMap.get(m);
			if (candidates == null || candidates.isEmpty())
				continue;

			double mWeight = weightCache.getMentionWeight(m,
					ELConfig.mPrefStreg);

			// We assume the only one candidate is the final true entity.
			if (candidates.size() == 1) {
				Entity finalEnt = candidates.keySet().iterator().next();
				ret.put(finalEnt.getName(), mWeight);
				continue;
			}

			Map<Entity, Double> tempMap = new HashMap<Entity, Double>();
			for (Entity e : candidates.keySet()) {
				double eWeight = weightCache.getEntityWeight(m, e,
						ELConfig.ePrefStreg);
				tempMap.put(e, eWeight);
			}

			ELUtils.normalize(tempMap);

			for (Entity e : tempMap.keySet()) {
				String entName = e.getName();
				if (ELConfig.mPrefStreg == ELConfig.PrefStrategy.RANDOM
						|| ELConfig.ePrefStreg == ELConfig.PrefStrategy.RANDOM)
					ret.put(entName, weightCache.getEntityWeight(m, e,
							ELConfig.PrefStrategy.RANDOM));
				else
					ret.put(entName, tempMap.get(e) * mWeight);
			}
		}

		return ret;
	}
    
    /**
     * Sort the mentions by their ambiguity so that the least ambiguous mention
     * is disambiguated first.
     * Here we simply use the number of candidates to measure the ambiguity of a mention.
     * In the future work, we may explore other measures such as Entropy of the mention.
     *
     * @param mentions
     * @param candMap
     * @return
     */
    private List<Mention> sortMentionByAmbiguity(List<Mention> mentions,
                                                 Map<Mention, Map<Entity, Double>> candMap) {

        Map<Entity, Double> candidates = null;
        List<Rank<Integer, Mention>> rankList = new ArrayList<Rank<Integer, Mention>>();
        for (Mention m : mentions) {
            int ambiguity = 0;
            candidates = candMap.get(m);
            if (candidates != null)
                ambiguity = candidates.size();

            rankList.add(new Rank<Integer, Mention>(ambiguity, m));
        }

        Collections.sort(rankList);

        List<Mention> ret = new ArrayList<Mention>();
        for (Rank<Integer, Mention> rank : rankList)
            ret.add(rank.obj);

        return ret;
    }
    
	private void normalizeFeatures(List<Feature> rankList) {
		Feature total = new Feature(0.0, 0.0, 0.0, 0.0);
		for (Feature rank : rankList) {
			total.prior += rank.prior;
			total.local += rank.local;
			total.semSim += rank.semSim;
			total.nameSim += rank.nameSim;
		}
		
		for (Feature rank : rankList) {
			if (total.prior > 0)
				rank.prior /= total.prior;
			if (total.local > 0)
				rank.local /= total.local;
			if (total.semSim > 0)
				rank.semSim /= total.semSim;
			if (total.nameSim > 0)
				rank.nameSim /= total.nameSim;
		}
	}
	
	private Entity disambiguateSupervised(List<Entity> candList, List<Feature> features) {
		int index = predictor.predict(features);
		
		if (index < 0)	return null;
		
		return candList.get(index);
	}
	
	private Entity disambiguateUnsupervised(List<Entity> candList, List<Feature> features) {
		normalizeFeatures(features);
		
		int maxIdx = 0;
		Feature max = features.get(0), f= null;
		for (int i = 1; i < features.size(); i++) {
			f = features.get(i);
			if (max.compareTo(f) < 0) {
				max = f;
				maxIdx = i;
			}
		}

		return candList.get(maxIdx);
	}
	

	private Entity disambiguateMention(Mention m,
			Map<Entity, Double> candidates, List<Double> docSemSig,
			Map<Integer, List<Double>> entSemSigs, Map<String, Integer> e2id,
			WeightCache weightCache) {
		if (candidates == null || candidates.isEmpty())
			return null;

		if (candidates.size() == 1)
			return candidates.keySet().iterator().next();

		List<Entity> entities = new ArrayList<Entity>();
		List<Feature> features = new ArrayList<Feature>();

		for (Entity ent : candidates.keySet()) {
			String entName = ent.getName();
			int eid = e2id.get(entName);

			double prior = candidates.get(ent);
			double local = weightCache.getEntityWeight(m, ent,
					ELConfig.PrefStrategy.CTX_SIM);
			double semSim = 1.0 / VectorSimilarity.ZeroKLDivergence(
					entSemSigs.get(eid), docSemSig);
			double nameSim = StringSim.ngram_distance(
					m.getName().toLowerCase(), entName.toLowerCase(), 2);

			entities.add(ent);
			features.add(new Feature(prior, local, semSim, nameSim));
		}

		// disambiguate
		Entity ret = null;
		if (ELConfig.supervised)
			ret = disambiguateSupervised(entities, features);
		else
			ret = disambiguateUnsupervised(entities, features);

		if (ret == null)
			LOGGER.info("[result]:" + "NIL");

		// Choose the entity with the highest ranking.
		// Remove the rest candidates.
		for (int i = 0; i < entities.size(); i++) {
			Entity ent = entities.get(i);
			Feature f = features.get(i);
			if (ent == ret) {
				LOGGER.info("[result]:" + ent.getName() + "\t"
						+ df.format(f.prior) + "\t" + df.format(f.local) + "\t"
						+ df.format(f.semSim) + "\t" + df.format(f.nameSim));

				continue;
			}

			LOGGER.info("[removed4]:" + ent.getName() + "\t"
					+ df.format(f.prior) + "\t" + df.format(f.local) + "\t"
					+ df.format(f.semSim) + "\t" + df.format(f.nameSim));

			candidates.remove(ent);
		}

		return ret;
	}

	private String linkingImplUnifiedIterative(Mention target, Document doc,
			List<Mention> mentions, String docName) {
		Map<Mention, Map<Entity, Double>> candMap = 
				selectCandidatesMention(target, doc, mentions, mentionCtxCache, entityCtxCache);
		if (candMap == null || candMap.isEmpty())
			return null;

		WeightCache weightCache = new WeightCache();
        //Cache the importance of mentions, context similarity and prior probability between mention and entity.
        weightCache.prepareWeightCache(ELConfig.mPrefStreg, mentions, candMap);
        weightCache.prepareContextSimCache(mentions, candMap);
        weightCache.preparePriorProbCache(mentions, candMap);

        //Use unambiguous mentions as the initial representation of the document.
        //This step has to be here, since we do some cleanup when we collect the unambiguous entities.
        Map<String, Double> unambigEntities = null;
        if (ELConfig.useUnambigEntity)
            unambigEntities = getUnambiguousEntities(candMap, weightCache);

        // Collect all entities for graph construction.
		Set<String> entities = collectNodes(candMap.keySet(), candMap);
		if (entities == null)
            entities = new HashSet<String>();

		// 2. Construct a graph including all candidate entities and the target Entities.
		Map<String, Integer> e2id = new HashMap<String, Integer>();
        PersonalizedPageRank ranker = null;
        
        if (ELConfig.weighted)
        	ranker = new WeightedPersonalizedPageRank(GraphUtils.buildWeightedGraph(gg, entities, e2id));
        else
        	ranker = new UnweightedPersonalizedPageRank(GraphUtils.buildUnweightedGraph(gg, entities, e2id));

        //3. Compute the semantic signature, and perform the disambiguation.
        //3.1. Compute the semantic signature of all entities.
		Map<Integer, List<Double>> entSemSigs =
                ELUtils.computePageRankParallel(entities, e2id, ranker);
		
		//clear the candidates by removing entities not in the graph.
        for (Mention m : mentions)
            cleanupCandidates(candMap.get(m), entities, e2id, entSemSigs);

		//compute the semantic signature of the document using targetEntities.
        Map<Entity, Double> candidates = null;

        //4. Iterative entity disambiguation.
        List<Mention> sortedMentions = sortMentionByAmbiguity(mentions, candMap);

        //find unambiguous mentions.
        for (Mention m : sortedMentions) {
            candidates = candMap.get(m);
            if (candidates == null || candidates.isEmpty()) {
            	LOGGER.info("[result]" + m.getName() + " : NIL");
            	if (m == target) return null;
            	
            	continue;
            }

            if (candidates.size() == 1) {
                Entity finalEnt = candidates.keySet().iterator().next();
                if (m == target)	return finalEnt.getName();
                
                double mWeight = weightCache.getMentionWeight(m, ELConfig.mPrefStreg);
                unambigEntities.put(finalEnt.getName(), mWeight);
            	LOGGER.info("[result]" + m.getName() + " : " + finalEnt.getName());
            }
        }

        List<Double> docSemSig = null;
        Map<String, Double> tempEntities = null;

        //put the target entity at the end of the list so that we can take advantage of the 
        //disambiguation results of other mentions to improve the accuracy on the target entity.
        sortedMentions.remove(target);
        sortedMentions.add(target);

        String result = null;
        
        //Start disambiguation.
        for (Mention m : sortedMentions) {
            candidates = candMap.get(m);
            if (candidates == null || candidates.size() < 2)
                continue;

            //Update the semantic signature of the document.
            if (unambigEntities != null && unambigEntities.size() > 0)
                tempEntities = unambigEntities;
            else
                //This case only happens for the first time when all mentions are ambiguous.
                tempEntities = getApproximateEntities(candMap, weightCache);

            // If there is any candidate in the unambigEntities,remove them and recompute the docSemSig.
            // The reason is that candidate in the unambigEntities will get higher semantic similarity with the doc.
            Set<String> avoidSet = new HashSet<String>();
            for (Entity ent : candidates.keySet()) {
                String name = ent.getName();
                if (tempEntities.containsKey(name)) {
                    LOGGER.info("Candidate in the representative entities: " + name);
                    avoidSet.add(name);
                }
            }

            docSemSig = ELUtils.computePageRank(tempEntities, avoidSet, e2id, ranker);
            Entity ent = disambiguateMention(m, candidates, docSemSig, entSemSigs, e2id, weightCache);
            if (ent == null) {
            	LOGGER.info("[result]" + m.getName() + " : NIL");
            } else {
            	unambigEntities.put(ent.getName(), weightCache.getMentionWeight(m, ELConfig.mPrefStreg));
            	LOGGER.info("[result]" + m.getName() + " : " + ent.getName());
            }
            
            if (m == target)
            	result = (ent == null ? null : ent.getName());
        }

		return result;
	}
	
	/**
	 * Use an iterative approach for the entity linking task.
	 */
	private String linkingImplUnified(Mention target, Document doc,
			List<Mention> mentions, String docName) {
		if (ELConfig.useIterative)
			return linkingImplUnifiedIterative(target, doc, mentions, docName);
/*		else
			return linkingImplUnifiedUniterative(mentions);
*/	
		return null;
	}


	public String linking(String targetFile) {
		//1. Load the query file into a dom tree.		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		org.w3c.dom.Document dom = null;
		
		try {
			//Using factory get an instance of document builder
			db = dbf.newDocumentBuilder();
			//parse using builder to get DOM representation of the XML file
			dom = db.parse(targetFile);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}

		//get the root element, and its nodes.
		Element rootEle = dom.getDocumentElement();
		NodeList docNL = rootEle.getElementsByTagName("query");
		if (docNL == null || docNL.getLength() <= 0) {
			LOGGER.warn(targetFile + ": docNL empty");
			return null;
		}
		
		
		int total = 0, correct = 0;
		for (int i = 0; i < docNL.getLength(); i++) {
			total++;
			LOGGER.info("==========" + (i+1) + "==============");
			//get the annotations of each document.
			Element docEle = (Element)docNL.item(i);
			//get the attribute <docName> of each document
			String mentionName = XmlProcessor.getTextValue(docEle, "name");
			String docid = XmlProcessor.getTextValue(docEle, "docid");
			String wikiName = XmlProcessor.getTextValue(docEle, "entity");
			String docPath = getPath(docid);
			LOGGER.info(mentionName + "[" + wikiName + "]:");
			LOGGER.info("\t" + docPath);

			//clear the data for re-use.
	        mentionCtxCache.clear();
	        entityCtxCache.clear();

			// Tokenize the document and get the index of each term.
			String content = ELUtils.readFile(docPath);
			Document doc = DocumentUtils.annotateDocument(content, ner, orthoMatcher);
			Map<Integer, Mention> idxMenMap = DocumentUtils.getIndex(doc);
			List<Mention> mentions = getMentions(doc);

			int offset = content.indexOf(mentionName);
			Mention mention = DocumentUtils.createMention(mentionName, offset,
					doc, idxMenMap);
/*			if (mention == null)
				mention = new Mention(
						new Entity(mentionName, mentionName),
						Entity.NONE, null, offset, offset + mentionName.length());
*/			
			mention = matchMention(mention, mentions);

			LOGGER.info("\t=====Mentions in the document=====");
			for (Mention m : mentions) {
				String name = m.getName();
				LOGGER.info("\t" + name + "[" + m.getEntity().getType() + "]" + ":" + m.getEntity().getName());
			}
			LOGGER.info("\t==================================");
			
			String result = null;
			if (mention != null)
				result = linkingImplUnified(mention, doc, mentions, docPath);
			if (result == null)
				LOGGER.info("\tNIL\t" + wikiName);
			else
				LOGGER.info("\t" + result + "\t" + wikiName);

			//Entity is in our new Wikipedia version, but not  in the old one.
			if (!entitySet.contains(result))
				result = null;
			
			if (result == null)
			{
				result = "NIL";
				if (wikiName.startsWith("NIL")) {
					correct++;
					LOGGER.info("\t[correct]");
				} else {
					LOGGER.info("\t[missed]");
				}
			} else if (wikiName.equalsIgnoreCase(result)) {
				correct++;
				LOGGER.info("\t[correct]");
			} else {
				LOGGER.info("\t[missed]");
			}
			
			Element newE = dom.createElement("result");
			if (result != null)
				newE.appendChild(dom.createTextNode(result));
			docEle.appendChild(newE);
		}

		//output to a file.
		String outFile = null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			outFile = new String(targetFile + ".l2r");
			
			DOMSource source = new DOMSource(dom);
			StreamResult result = new StreamResult(new File(outFile));
			
			transformer.transform(source, result);
		} catch (Exception ie) {
			ie.printStackTrace();
		}

		LOGGER.info("Accuracy: " + correct*1.0/total);
		return outFile;
	}
	
	/**
	 * java TACSemanticEL tac11.config tac2011-el-queries-new.xml tac-kb-wiki.map 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TACSemanticEL linker = new TACSemanticEL(args[0]);
		
		// /nfs/data1/zhaochen/el-dataset/tac-kbp/tac-kb-wiki.map
		linker.loadKBEntities(args[2]);
		
		//  /nfs/data1/zhaochen/el-dataset/tac-kbp/tac2011-el-queries-new.xml
		linker.linking(args[1]);
	}

}
