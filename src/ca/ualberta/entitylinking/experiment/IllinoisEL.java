package ca.ualberta.entitylinking.experiment;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ualberta.entitylinking.config.ELConfig;
import edu.illinois.cs.cogcomp.wikifier.ReferenceAssistant;
import edu.illinois.cs.cogcomp.wikifier.models.ReferenceInstance;

/**
 * This class is a wraper over the Illinois Entity linking system, aiming to run the 
 * Illinois EL system over our data format.
 * 
 * @author zhaochen
 *
 */
public class IllinoisEL {
	private static String configDir = "/nfs/data1/zhaochen/projects/Wikifier2013/configs";
	private String configFile = "BASELINE.xml";

	public IllinoisEL(String config) {
		configFile = configDir + "/" + config + ".xml";
		System.out.println("ConfigFile: " + configFile);
	}
	
	public void linking(String fileName) throws Exception {
		//get the RawText directory.
		String dir = fileName.substring(0, fileName.lastIndexOf('/'));
		ELConfig.DATASET_DIR = dir + "/RawText/";

		String problemDir = "wikifyProblems";
		String outputDir = "wikifierOutput";
		String scriptPath = "/nfs/data1/zhaochen/el-dataset/create_problem.py";
		
		//create the problem directory and output directory first.
		File f = new File(problemDir);
		if (f.exists() && f.isDirectory())
			Runtime.getRuntime().exec("rm -rf " + problemDir).waitFor();
		Runtime.getRuntime().exec("mkdir " + problemDir).waitFor();
		f = new File(outputDir);
		if (f.exists() && f.isDirectory())
			Runtime.getRuntime().exec("rm -rf " + outputDir).waitFor();
		Runtime.getRuntime().exec("mkdir " + outputDir).waitFor();
		
		//1. Parse the input file and convert it to the problem format defined by the Wikifier.
		// Here we use a python script to create the problems.
		String command = "python3 " + scriptPath + " " + fileName + " " + problemDir;
		Runtime.getRuntime().exec(command).waitFor();
		
		//2. Use the Wikifier to do the entity linking.
		String options[] = {"-referenceAssistant", problemDir, ELConfig.DATASET_DIR, outputDir, configFile};
		ReferenceAssistant.main(options);
		
		System.out.println("!!!Done with the wikification!!!");
		
		//3. Measure the performance.
		int totalQueries = 0;
		int totalCorrect = 0;
		int totalFound = 0;
		int totalDocs = 0;
		double precision = 0.0, recall = 0.0, f1 = 0.0;

		f = new File(problemDir);
		File[] fileList = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return !name.equals(".") && !name.equals("..");
			}
		});

		for (File file : fileList) {
			Map<String, String> truthMap = new HashMap<String, String>();
			
			//Read problem first.
			List<ReferenceInstance> instances = 
					ReferenceInstance.loadReferenceProblem(file.getAbsolutePath());
			for (ReferenceInstance ri : instances) {
				String key = ri.surfaceForm + "_" + ri.charStart + "_" + ri.charLength;
				String value = ri.chosenAnnotation;
				truthMap.put(key, value);
			}
			
			int correct = 0, query = 0, found = 0;

			//read the results, and measure the performance.
			String outputFile = 
					outputDir + "/" + file.getName() + ".referenceInstance.tagged";
			instances = ReferenceInstance.loadReferenceProblem(outputFile);
			for (ReferenceInstance ri : instances) {
				String key = ri.surfaceForm + "_" + ri.charStart + "_" + ri.charLength;
				String value = ri.chosenAnnotation;
				
				totalQueries++;
				query++;
				
				if (!value.equals("*null*") || value.equals(truthMap.get(key))) {
					totalFound++;
					found++;
				}
				
				if (value.equals(truthMap.get(key))) {
					totalCorrect++;
					correct++;
				}
			}
			
			if (query == 0)	continue;
			if (found == 0)
				precision += 0;
			else
				precision += correct * 1.0 / found;
			
			recall += correct * 1.0 / query;
			
			totalDocs++;
		}
		
		System.out.println("Acuracy: " + (totalCorrect*1.0/totalQueries));

		precision = precision / totalDocs;
		recall = recall / totalDocs;
		f1 = 2 * precision * recall / (precision + recall);
		
		System.out.println("MA#Precision: " + precision);
		System.out.println("MA#Recall: " + recall);
		System.out.println("MA#F1: " + f1);
		
		precision = totalCorrect * 1.0 / totalFound;
		recall = totalCorrect * 1.0 / totalQueries;
		f1 = 2 * precision * recall / (precision + recall);

		System.out.println("MI#Precision: " + precision);
		System.out.println("MI#Recall: " + recall);
		System.out.println("MI#F1: " + f1);
	}
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		IllinoisEL obj = new IllinoisEL(args[1]);

		obj.linking(args[0]);
	}
}
