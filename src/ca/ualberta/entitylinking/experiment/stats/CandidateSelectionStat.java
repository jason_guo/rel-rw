package ca.ualberta.entitylinking.experiment.stats;

import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import ca.ualberta.entitylinking.utils.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ca.ualberta.entitylinking.common.data.Document;
import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.common.data.Mention;
import ca.ualberta.entitylinking.common.nlp.OrthoMatcherCoref;
import ca.ualberta.entitylinking.common.nlp.StanfordNER;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;
import ca.ualberta.entitylinking.common.indexing.Tokenizer;
import ca.ualberta.entitylinking.cs.CandidateSelection;
import ca.ualberta.entitylinking.cs.CandidateSelectionLuceneSimple;
import ca.ualberta.entitylinking.config.ELConfig;

/**
 * @author zhaochen
 *
 */
public class CandidateSelectionStat {
	private static Logger LOGGER = LogManager.getLogger(CandidateSelectionStat.class);
    public static DecimalFormat df = new DecimalFormat("#.###");

	protected StanfordNER ner = null;
	protected OrthoMatcherCoref orthoMatcher = null;
	protected TFIDF3x tfidfIndex = null;
	protected Tokenizer toker = null;
	protected CandidateSelection cs = null;
    
	public CandidateSelectionStat() {
		ELConfig.loadConfiguration("el.config");
        //Create the NER and co-reference resolution components.
		orthoMatcher = new OrthoMatcherCoref();
		Set<String> allowedEntityTypes = new HashSet<String>();
		allowedEntityTypes.add(Entity.PERSON);
		allowedEntityTypes.add(Entity.ORGANIZATION);
		allowedEntityTypes.add(Entity.LOCATION);
		allowedEntityTypes.add(Entity.MISC);
		ner = new StanfordNER(allowedEntityTypes);
        LOGGER.info(ELUtils.currentTime() + "Done with loading StanfordNER and GATE OrthoMatcher");

        //Candidate selection
		cs = new CandidateSelectionLuceneSimple();
        LOGGER.info(ELUtils.currentTime() + "Done with loading lucene index");

		try {
            tfidfIndex = new TFIDF3x();
            LOGGER.info(ELUtils.currentTime() + "Done with loading the TFIDF index");
			toker = new Tokenizer();
        } catch (Exception e) {
			e.printStackTrace();
		}
	}

    public Map<Mention, Map<Entity, Double>> selectCandidatesMention(
            List<Mention> mentions) {
        if (mentions == null || mentions.isEmpty())
            return null;

        Map<Mention, Map<Entity, Double>> ret = new HashMap<Mention, Map<Entity, Double>>();
        Map<Entity, Double> candidates = null;
        for (Mention m : mentions) {
        	if (m == null || m.getEntity() == null)
        		continue;
            String name = m.getEntity().getName();
            candidates = cs.selectCandidates(name);
            if (candidates == null || candidates.isEmpty()) {
            	LOGGER.info("No candidates!");
                continue;
            }

            ret.put(m, candidates);
        }

        return ret;
    }


	public void getCandidateSelectionStats(String file) {
		//1. Load the query file into a DOM tree.
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		org.w3c.dom.Document dom = null;
		PrintStream output = null;
		
		try {
			output = new PrintStream(file + ".stats");
			//Using factory get an instance of document builder
			db = dbf.newDocumentBuilder();
			//parse using builder to get DOM representation of the XML file
			dom = db.parse(file);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}

		//get the root element, and its nodes.
		Element rootEle = dom.getDocumentElement();
		NodeList docNL = rootEle.getElementsByTagName("document");
		if (docNL == null || docNL.getLength() <= 0) {
			LOGGER.warn("docNL empty");
			return;
		}

        String dir = file.substring(0, file.lastIndexOf('/'));
        String DATASET_DIR = dir + "/RawText/";

		List<Mention> mentions = new ArrayList<Mention>();

		for (int i = 0; i < docNL.getLength(); i++) {
			//get the annotations of each document.
			Element docEle = (Element)docNL.item(i);
			//get the attribute <docName> of each document
			String docName = docEle.getAttribute("docName");
			//get a node list of <annotation>
			NodeList annoteNL = docEle.getElementsByTagName("annotation");
			if (annoteNL == null || annoteNL.getLength() <= 0)
				continue;

			LOGGER.info("[doc]: " + docName);
			//Tokenize the document and get the index of each term.
			String content = ELUtils.readFile(DATASET_DIR + "/" + docName);
			// Annotate document.
			Document doc = DocumentUtils.annotateDocument(content, ner, orthoMatcher);

			Map<Integer, Mention> idxMenMap = DocumentUtils.getIndex(doc);

			//clear the data for re-use.
			mentions.clear();;

			for (int j = 0; j < annoteNL.getLength(); j++) {
				Element annoteEle = (Element) annoteNL.item(j);
				
				String mentionName = XmlProcessor.getTextValue(annoteEle, "mention");
				String wikiName = XmlProcessor.getTextValue(annoteEle, "wikiName");
				int offset = XmlProcessor.getIntValue(annoteEle, "offset");
				
				if (mentionName == null || mentionName.isEmpty())
					continue;
				
				if (wikiName != null && wikiName.equals("NIL"))
					wikiName = null;
				
				if (wikiName != null && (wikiName.equals("NIL") || wikiName.isEmpty()))
					wikiName = null;

				Mention m = DocumentUtils.createMention(mentionName, offset, doc, idxMenMap);
				mentions.add(m);
			}

//            ELUtils.resolve(mentions);

            //Select candidates.
            Map<Mention, Map<Entity, Double>> candMap = selectCandidatesMention(mentions);
            if (candMap == null || candMap.isEmpty())
                continue;
            
            int candNum = 0;
            for (Mention m : candMap.keySet()) {
            	Map<Entity, Double> candidates = candMap.get(m);
            	if (candidates != null)
            		candNum += candidates.size();
            }
            
            output.println(docName + "\t" + candMap.size() + "\t" + candNum + "\t" + (candNum*1.0/candMap.size()));
		}
		
		output.close();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CandidateSelectionStat obj = new CandidateSelectionStat();

		obj.getCandidateSelectionStats(args[0]);
	}
}
