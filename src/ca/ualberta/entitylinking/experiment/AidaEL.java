package ca.ualberta.entitylinking.experiment;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import mpi.aida.AidaManager;
import mpi.aida.Disambiguator;
import mpi.aida.Preparator;
import mpi.aida.config.settings.DisambiguationSettings;
import mpi.aida.config.settings.PreparationSettings;
import mpi.aida.config.settings.disambiguation.CocktailPartyDisambiguationSettings;
import mpi.aida.config.settings.preparation.StanfordManualPreparationSettings;
import mpi.aida.data.DisambiguationResults;
import mpi.aida.data.PreparedInput;
import mpi.aida.data.ResultEntity;
import mpi.aida.data.ResultMention;

import ca.ualberta.entitylinking.config.ELConfig;
import ca.ualberta.entitylinking.utils.ELUtils;
import ca.ualberta.entitylinking.utils.XmlProcessor;

/**
 * Wrap the AIDA entity linking, and verify its performance on
 * our datasets. One supported input is the text with manually 
 * extracted named entities. So what we do here is to convert
 * out file into their manual type.
 * 
 * @author zhaochen
 *
 */
public class AidaEL {
	Map<String, Element> truthMap = new HashMap<String, Element>(); 

	public AidaEL() {
		
	}
	
	private String formatContent(Element docEle) {
		//get the attribute <docName> of each document
		String docName = docEle.getAttribute("docName");
		//get a node list of <annotation>
		NodeList annoteNL = docEle.getElementsByTagName("annotation");
		if (annoteNL == null || annoteNL.getLength() <= 0)
			return null;

		//tokenize the document and get the index of each term.
		String content = ELUtils.readFile(ELConfig.DATASET_DIR + docName);
		StringBuilder newDoc = new StringBuilder();
		int sPos = 0, ePos = 0;
		for (int j = 0; j < annoteNL.getLength(); j++) {
			Element annoteEle = (Element) annoteNL.item(j);
			
			String mentionName = XmlProcessor.getTextValue(annoteEle, "mention");
			String wikiName = XmlProcessor.getTextValue(annoteEle, "wikiName");
			int offset = XmlProcessor.getIntValue(annoteEle, "offset");
			
			//we ignore the queries whose wikiName is null (linked to NIL).
			if (wikiName == null || wikiName.isEmpty() || 
				mentionName == null || mentionName.isEmpty())
				continue;
			
			ePos = offset;
			newDoc.append(content.substring(sPos, ePos));
			newDoc.append("[[");
			sPos = ePos;
			ePos = offset + mentionName.length();
			newDoc.append(content.substring(sPos, ePos));
			newDoc.append("]]");
			sPos = ePos;
			
			String key = mentionName + "-" + Integer.toString(offset);
			truthMap.put(key, annoteEle);
		}
		
		return newDoc.toString();
	}
	
	public static String getWikiName(String wikiUrl) {
		if (wikiUrl == null || wikiUrl.isEmpty())
			return null;
		
		String name = wikiUrl.substring(wikiUrl.lastIndexOf("/")+1);
		if (name == null)
			return name;
		
		return name.replace('_', ' ');
	}
	
	public DisambiguationResults AidaLinkingImpl(String content) throws Exception {
		// Prepare the input for disambiguation. 
		//Strings marked with [[ ]] will be treated as mentions.
		PreparationSettings prepSettings = new StanfordManualPreparationSettings();
		Preparator p = new Preparator();
		PreparedInput input = p.prepare(content, prepSettings);

		// Disambiguate the input with the graph coherence algorithm.
		DisambiguationSettings disSettings = new CocktailPartyDisambiguationSettings();    
//		DisambiguationSettings disSettings = new CocktailPartyKOREDisambiguationSettings();    
		Disambiguator d = new Disambiguator(input, disSettings);
		DisambiguationResults results = d.disambiguate();

		return results;
	}
	
	/**
	 * Link files using the AIDA entity linking system.
	 * 
	 * @param fileName
	 * @return
	 */
	public String linking(String fileName) {
		//get the RawText directory.
		String dir = fileName.substring(0, fileName.lastIndexOf('/'));
		ELConfig.DATASET_DIR = dir + "/RawText/";

		//1. Load the query file into a dom tree.		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		org.w3c.dom.Document dom = null;

		try {
			//Using factory get an instance of document builder
			db = dbf.newDocumentBuilder();
			//parse using builder to get DOM representation of the XML file
			dom = db.parse(fileName);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}

		//get the root element, and its nodes.
		Element rootEle = dom.getDocumentElement();
		NodeList docNL = rootEle.getElementsByTagName("document");
		if (docNL == null || docNL.getLength() <= 0) {
			System.out.println("docNL empty");
			return null;
		}
		
		for (int i = 0; i < docNL.getLength(); i++) {
			String content = formatContent((Element)docNL.item(i));
			//Call the AIDA system.
			try {
				DisambiguationResults results = AidaLinkingImpl(content);
				
				// Print the disambiguation results.
				for (ResultMention rm : results.getResultMentions()) {
				  ResultEntity re = results.getBestEntity(rm);

				  String key = rm.getMention() + "-" + Integer.toString(rm.getCharacterOffset());
				  Element ele = truthMap.get(key);
				  String entName = getWikiName(AidaManager.getWikipediaUrl(re));
				  Element newE = dom.createElement("entity");
				  if (entName != null && !entName.contains("OOKBE"))
					newE.appendChild(dom.createTextNode(entName));
				  ele.appendChild(newE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//output to a file.
		String outFile = null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			outFile = new String(fileName + ".result.AIDA");

			DOMSource source = new DOMSource(dom);
			StreamResult result = new StreamResult(new File(outFile));
			
			transformer.transform(source, result);
		} catch (Exception ie) {
			ie.printStackTrace();
		}
		
		return outFile;

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		AidaEL aida = new AidaEL();
		String outFile = aida.linking(args[0]);

		// report the accuracy of the entity linking.
		Evaluation.accuracy(outFile);
	}
}
