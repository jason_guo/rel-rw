package ca.ualberta.entitylinking.experiment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ca.ualberta.entitylinking.config.ELConfig;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.wikipedia.miner.annotation.Context;
import org.wikipedia.miner.annotation.Disambiguator;
import org.wikipedia.miner.annotation.Topic;
import org.wikipedia.miner.annotation.TopicReference;
import org.wikipedia.miner.annotation.preprocessing.PreprocessedDocument;
import org.wikipedia.miner.model.Article;
import org.wikipedia.miner.model.Label;
import org.wikipedia.miner.model.Page.PageType;
import org.wikipedia.miner.model.Wikipedia;
import org.wikipedia.miner.util.Position;
import org.wikipedia.miner.util.RelatednessCache;
import org.wikipedia.miner.util.WikipediaConfiguration;
import org.xml.sax.SAXException;

import ca.ualberta.entitylinking.utils.ELUtils;
import ca.ualberta.entitylinking.utils.XmlProcessor;

/**
 * Use the Wikipedia Miner 1.2 for the Entity Linking.
 * 
 * @author zhaochen
 *
 */
public class WikipediaMinerEL {
	private static Logger LOGGER = LogManager.getLogger(WikipediaMinerEL.class);
	
	private Wikipedia wikipedia ;
	private Disambiguator disambiguator ;
	
	private boolean strictDisambiguation = true;
	private boolean allowDisambiguations = false;
	
	private int maxTopicsForRelatedness = 25 ;
	
	private List<String> truth = null;

	public WikipediaMinerEL() {
		ELConfig.wikiConfigFile = "/nfs/data1/zhaochen/wikipedia-miner-1.2.0/configs/wikipedia-20130604-wm.xml";
		
		try {
			WikipediaConfiguration conf = new WikipediaConfiguration(new File(ELConfig.wikiConfigFile));
			wikipedia = new Wikipedia(conf, false);
			disambiguator = new Disambiguator(wikipedia) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Vector<Topic> getTopics(PreprocessedDocument doc, RelatednessCache rc) throws Exception {
		
		if (rc == null)
			rc = new RelatednessCache(disambiguator.getArticleComparer()) ;
		
		Vector<TopicReference> references = getReferences(doc.getPreprocessedText()) ;
		
		Collection<Topic> temp = getTopics(references, doc.getContextText(), doc.getOriginalText().length(), rc).values() ;
		calculateRelatedness(temp, rc) ;

		Vector<Topic> topics = new Vector<Topic>() ;
		for (Topic t:temp) {
			if (!doc.isTopicBanned(t.getId())) 
					topics.add(t) ;
		}
		
		return topics ;
	}

	public Collection<Topic> getTopics(String text, RelatednessCache rc) throws Exception {
		
		if (rc == null)
			rc = new RelatednessCache(disambiguator.getArticleComparer()) ;

		//Vector<String> sentences = ss.getSentences(text, SentenceSplitter.MULTIPLE_NEWLINES) ;
		Vector<TopicReference> references = getReferences(text) ;
		
		HashMap<Integer,Topic> topicsById = getTopics(references, "", text.length(), rc) ;

		Collection<Topic> topics = topicsById.values() ;
		calculateRelatedness(topics, rc) ;
		
		return topics ;
	}

	private void calculateRelatedness(Collection<Topic> topics, RelatednessCache cache) throws Exception{
		
		TreeSet<Article> weightedTopics = new TreeSet<Article>() ;
		
		for (Topic t:topics) {
			if (t.getType() != PageType.article)
				continue ;
			
			Article art = (Article)wikipedia.getPageById(t.getId()) ;
			
			art.setWeight(t.getAverageLinkProbability() * t.getOccurances()) ;
			weightedTopics.add(art) ;
		}
		
		for (Topic topic: topics) {
			
			double totalWeight = 0 ;
			double totalWeightedRelatedness = 0 ;
			
			int count = 0 ;
			
			for (Article art: weightedTopics) {
				if (count++ > maxTopicsForRelatedness)
					break ;
				
				double weightedRelatedness = art.getWeight() * cache.getRelatedness(topic, art) ;
				
				totalWeight = totalWeight + art.getWeight();
				totalWeightedRelatedness = totalWeightedRelatedness + weightedRelatedness;
				
			}
			
			topic.setRelatednessToOtherTopics((float)(totalWeightedRelatedness/totalWeight)) ;
		}
	}
	
	private Vector<TopicReference> getReferences(String text) {

		Vector<TopicReference> references = new Vector<TopicReference>() ;
		//int sentenceStart = 0 ;

		//ProgressDisplayer pd = new ProgressDisplayer(" - gathering candidate links", sentences.length, 0.1) ;

		//for (String sentence:sentences) {
			String s = "$ " + text + " $" ;
			//pd.update() ;

			Pattern p = Pattern.compile("[\\s\\{\\}\\(\\)\"\'\\.\\,\\;\\:\\-\\_]") ;  //would just match all non-word chars, but we don't want to match utf chars
			Matcher m = p.matcher(s) ;

			Vector<Integer> matchIndexes = new Vector<Integer>() ;

			while (m.find()) 
				matchIndexes.add(m.start()) ;

			for (int i=0 ; i<matchIndexes.size() ; i++) {

				int startIndex = matchIndexes.elementAt(i) + 1 ;
				
				if (Character.isWhitespace(s.charAt(startIndex))) 
					continue ;

				for (int j=Math.min(i + disambiguator.getMaxLabelLength(), matchIndexes.size()-1) ; j > i ; j--) {
					int currIndex = matchIndexes.elementAt(j) ;	
					String ngram = s.substring(startIndex, currIndex) ;

					if (! (ngram.length()==1 && s.substring(startIndex-1, startIndex).equals("'"))&& !ngram.trim().equals("") && !wikipedia.getConfig().isStopword(ngram)) {
						
						//TODO: test if we need escapes here
						Label label = new Label(wikipedia.getEnvironment(), ngram, disambiguator.getTextProcessor()) ;

						if (label.exists() && label.getLinkProbability() >= disambiguator.getMinLinkProbability()) {
							Position pos = new Position(startIndex-2, currIndex-2) ;
							TopicReference ref = new TopicReference(label, pos) ;
							references.add(ref) ;
							
							//System.out.println(" - ref: " + ngram + label.getLinkProbability()) ;
						}
					}
				}
			}
			//sentenceStart = sentenceStart + sentence.length() ;
		//}
		return references ;
	}
	
	private HashMap<Integer,Topic> getTopics(Vector<TopicReference> references, String contextText, int docLength, RelatednessCache cache) throws Exception{
		HashMap<Integer,Topic> chosenTopics = new HashMap<Integer,Topic>() ;
	
		// get context articles from unambiguous Labels
		Vector<Label> unambigLabels = new Vector<Label>() ;
		for (TopicReference ref:references) {
			Label label = ref.getLabel() ;
			
			Label.Sense[] senses = label.getSenses() ;
			if (senses.length > 0) {				
				if (senses.length == 1 || senses[0].getPriorProbability() > 1-disambiguator.getMinSenseProbability())
					unambigLabels.add(label) ;	
			}		
		}
		
		//get context articles from additional context text
		//Vector<String> contextSentences = ss.getSentences(, SentenceSplitter.MULTIPLE_NEWLINES) ; 
		for (TopicReference ref:getReferences(contextText)){
			Label label = ref.getLabel() ;
			Label.Sense[] senses = label.getSenses() ;
			if (senses.length > 0) {
				if (senses.length == 1 || senses[0].getPriorProbability() > 1-disambiguator.getMinSenseProbability()) {
					unambigLabels.add(label) ;
				}
			}
		}
		
		Context context ;
		if (cache == null)
			context = new Context(unambigLabels, new RelatednessCache(disambiguator.getArticleComparer()), disambiguator.getMaxContextSize()) ;
		
		else 
			context = new Context(unambigLabels, cache, disambiguator.getMaxContextSize()) ;	
		unambigLabels = null ;

		//now disambiguate all references
		//unambig references are still processed here, because we need to calculate relatedness to context anyway.
		
		// build a cache of valid senses for each phrase, since the same phrase may occur more than once, but will always be disambiguated the same way
		HashMap<String, ArrayList<CachedSense>> disambigCache = new HashMap<String, ArrayList<CachedSense>>() ;

		for (TopicReference ref:references) {
			//System.out.println("disambiguating ref: " + ref.getLabel().getText()) ;

			ArrayList<CachedSense> validSenses = disambigCache.get(ref.getLabel().getText()) ;

			if (validSenses == null) {
				// we havent seen this label in this document before
				validSenses = new ArrayList<CachedSense>() ;

				for (Label.Sense sense: ref.getLabel().getSenses()) {
					
					if (sense.getPriorProbability() < disambiguator.getMinSenseProbability()) break ;
					
					if (!allowDisambiguations && sense.getType() == PageType.disambiguation)
						continue ;

					double relatedness = context.getRelatednessTo(sense) ;
					double commonness = sense.getPriorProbability() ;

					double disambigProb = disambiguator.getProbabilityOfSense(commonness, relatedness, context) ;

					//System.out.println(" - sense " + sense + ", " + disambigProb) ;
					
					if (disambigProb > 0.1) {
						// there is at least a chance that this is a valid sense for the link (there may be more than one)
						
						CachedSense vs = new CachedSense(sense.getId(), commonness, relatedness, disambigProb) ;
						validSenses.add(vs) ;
					}
				}
				Collections.sort(validSenses) ;
				
				
				disambigCache.put(ref.getLabel().getText(), validSenses) ;
			}

			if (strictDisambiguation) {
				//just get top sense
				if (!validSenses.isEmpty()) {
					CachedSense sense = validSenses.get(0) ;
					Topic topic = chosenTopics.get(sense.id) ;
	
					if (topic == null) {
						// we havent seen this topic before
						topic = new Topic(wikipedia, sense.id, sense.relatedness, docLength) ;
						chosenTopics.put(sense.id, topic) ;
					}
					topic.addReference(ref, sense.disambigConfidence) ;
				}
			} else {
				//get all senses
				for (CachedSense sense: validSenses) {
					Topic topic = chosenTopics.get(sense.id) ;

					if (topic == null) {
						// we haven't seen this topic before
						topic = new Topic(wikipedia, sense.id, sense.relatedness, docLength) ;
						chosenTopics.put(sense.id, topic) ;
					}
					topic.addReference(ref, sense.disambigConfidence) ;
				}
			}
		}
		
		return chosenTopics ;
	}

	private class CachedSense implements Comparable<CachedSense>{
		
		int id ;
		double commonness ;
		double relatedness ;
		double disambigConfidence ;

		/**
		 * Initializes a new CachedSense
		 * 
		 * @param id the id of the article that represents this sense
		 * @param commonness the prior probability of this sense given a source ngram (label)
		 * @param relatedness the relatedness of this sense to the surrounding unambiguous topics
		 * @param disambigConfidence the probability that this sense is valid, as defined by the disambiguator.
		 */
		public CachedSense(int id, double commonness, double relatedness, double disambigConfidence) {
			this.id = id ;
			this.commonness = commonness ;
			this.relatedness = relatedness ;
			this.disambigConfidence = disambigConfidence ;			
		}
		
		public int compareTo(CachedSense sense) {
			return -1 * Double.valueOf(disambigConfidence).compareTo(Double.valueOf(sense.disambigConfidence)) ;
		}
	}

	private List<String> linkingWM(List<TopicReference> references, String contextText,
			double docLength) throws Exception {
		List<String> ret = new ArrayList<String>();
		
		RelatednessCache cache = new RelatednessCache(disambiguator.getArticleComparer()) ;

		HashMap<Integer,Topic> chosenTopics = new HashMap<Integer,Topic>() ;
		
		// get context articles from unambiguous Labels
		Vector<Label> unambigLabels = new Vector<Label>() ;
		for (TopicReference ref:references) {
			Label label = ref.getLabel() ;
			
			Label.Sense[] senses = label.getSenses() ;
			if (senses.length > 0) {				
				if (senses.length == 1 || senses[0].getPriorProbability() > 1-disambiguator.getMinSenseProbability())
					unambigLabels.add(label) ;	
			}		
		}
		
		//get context articles from additional context text
		//Vector<String> contextSentences = ss.getSentences(, SentenceSplitter.MULTIPLE_NEWLINES) ; 
//		for (TopicReference ref:getReferences(contextText)){
//			Label label = ref.getLabel() ;
//			Label.Sense[] senses = label.getSenses() ;
//			if (senses.length > 0) {
//				if (senses.length == 1 || senses[0].getPriorProbability() > 1-disambiguator.getMinSenseProbability()) {
//					unambigLabels.add(label) ;
//				}
//			}
//		}
		
		Context context = new Context(unambigLabels, cache, disambiguator.getMaxContextSize());	
		unambigLabels = null ;

		//now disambiguate all references
		//unambig references are still processed here, because we need to calculate relatedness to context anyway.
		
		// build a cache of valid senses for each phrase, since the same phrase may occur more than once, but will always be disambiguated the same way
		HashMap<String, ArrayList<CachedSense>> disambigCache = new HashMap<String, ArrayList<CachedSense>>() ;

		for (TopicReference ref:references) {
			//System.out.println("disambiguating ref: " + ref.getLabel().getText()) ;
			//check if the lable exists in the alias dictionary.
			Label label = ref.getLabel();
			if (label == null || !label.exists() ||
					label.getLinkProbability() < disambiguator.getMinLinkProbability()) {
				ret.add(null);
				
				if (label == null)
					System.out.println("Label == NULL");
				else if (!label.exists())
					System.out.println(label.getText() + "[!exists]:" + label.getLinkProbability() + " < " +
							disambiguator.getMinLinkProbability());
				else
					System.out.println(label.getText() + "[priorProb]:" + label.getLinkProbability() + " < " +
							disambiguator.getMinLinkProbability());
					
				continue;
			}

			ArrayList<CachedSense> validSenses = disambigCache.get(ref.getLabel().getText()) ;

			if (validSenses == null) {
				// we havent seen this label in this document before
				validSenses = new ArrayList<CachedSense>() ;

				for (Label.Sense sense: ref.getLabel().getSenses()) {
					
					if (sense.getPriorProbability() < disambiguator.getMinSenseProbability()) break ;
					
					if (!allowDisambiguations && sense.getType() == PageType.disambiguation)
						continue ;

					double relatedness = context.getRelatednessTo(sense) ;
					double commonness = sense.getPriorProbability() ;

					double disambigProb = disambiguator.getProbabilityOfSense(commonness, relatedness, context) ;

					//System.out.println(" - sense " + sense + ", " + disambigProb) ;
					
					if (disambigProb > 0.1) {
						// there is at least a chance that this is a valid sense for the link (there may be more than one)
						
						CachedSense vs = new CachedSense(sense.getId(), commonness, relatedness, disambigProb) ;
						validSenses.add(vs) ;
					}
				}
				Collections.sort(validSenses) ;
				
				
				disambigCache.put(ref.getLabel().getText(), validSenses) ;
			}

			// just get top sense
			if (!validSenses.isEmpty()) {
				CachedSense sense = validSenses.get(0);
				Topic topic = chosenTopics.get(sense.id);

				if (topic == null) {
					// we havent seen this topic before
					topic = new Topic(wikipedia, sense.id, sense.relatedness,
							docLength);
					chosenTopics.put(sense.id, topic);
				}
				
				topic.addReference(ref, sense.disambigConfidence);
				String title = topic.getTitle();
				ret.add(title);
			} else {
				ret.add(null);
				System.out.println(label.getText() + "[NO valid sense]");
			}
		}
		
		return ret;
	}

	public String linking(String file) {
		//get the RawText directory.
		String dir = file.substring(0, file.lastIndexOf('/'));
		ELConfig.DATASET_DIR = dir + "/RawText/";

		//1. Load the query file into a dom tree.		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		org.w3c.dom.Document dom = null;
		
		try {
			//Using factory get an instance of document builder
			db = dbf.newDocumentBuilder();
			//parse using builder to get DOM representation of the XML file
			dom = db.parse(file);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}

		//get the root element, and its nodes.
		Element rootEle = dom.getDocumentElement();
		NodeList docNL = rootEle.getElementsByTagName("document");
		if (docNL == null || docNL.getLength() <= 0) {
			LOGGER.warn("docNL empty");
			return null;
		}
		
		for (int i = 0; i < docNL.getLength(); i++) {
			//get the annotations of each document.
			Element docEle = (Element)docNL.item(i);
			//get the attribute <docName> of each document
			String docName = docEle.getAttribute("docName");
			//get a node list of <annotation>
			NodeList annoteNL = docEle.getElementsByTagName("annotation");
			if (annoteNL == null || annoteNL.getLength() <= 0)
				continue;

			//For each document, create the list of Mentions (TopicReference).
			List<TopicReference> references = new ArrayList<TopicReference>() ;
			
			String content = ELUtils.readFile(ELConfig.DATASET_DIR + docName);
			List<Element> elements = new ArrayList<Element>();
			
			truth = new ArrayList<String>();
			
			for (int j = 0; j < annoteNL.getLength(); j++) {
				Element annoteEle = (Element) annoteNL.item(j);
				
				String mentionName = XmlProcessor.getTextValue(annoteEle, "mention");
				String wikiName = XmlProcessor.getTextValue(annoteEle, "wikiName");
				int offset = XmlProcessor.getIntValue(annoteEle, "offset");
				int length = XmlProcessor.getIntValue(annoteEle, "length");
				
				if (wikiName.equals("NIL"))
					wikiName = null;
				//we ignore the queries whose wikiName is null (linked to NIL).
				if (wikiName == null || wikiName.isEmpty() || 
					mentionName == null || mentionName.isEmpty())
					continue;
				
				elements.add(annoteEle);
				truth.add(wikiName);
				
				//create a new mention.
				Label label = new Label(wikipedia.getEnvironment(), mentionName, disambiguator.getTextProcessor());
				Position pos = new Position(offset, offset+length) ;
				TopicReference ref = new TopicReference(label, pos) ;
				references.add(ref) ;
			}
			
			if (elements == null || elements.isEmpty())
				continue;
			
			LOGGER.info("==================");
			List<String> results = null;
			
			try {
				results = linkingWM(references, content, (double)content.length());
			} catch (Exception e){
				e.printStackTrace();
			}
			
			for (int j = 0; j < elements.size(); j++) {
				Element ele = elements.get(j);
				String entName = results.get(j);
				
				Element newE = dom.createElement("entity");
				if (entName != null)
					newE.appendChild(dom.createTextNode(entName));
				ele.appendChild(newE);
			}
		}
		
		//output to a file.
		String outFile = null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			outFile = new String(file + ".wikipedia-miner");
			
			DOMSource source = new DOMSource(dom);
			StreamResult result = new StreamResult(new File(outFile));
			
			transformer.transform(source, result);
		} catch (Exception ie) {
			ie.printStackTrace();
		}
		
		return outFile;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		WikipediaMinerEL obj = new WikipediaMinerEL();
		String outFile = obj.linking(args[0]);

		// report the accuracy of the entity linking.
		Evaluation.accuracy(outFile);
	}
}

