package ca.ualberta.entitylinking.cs;

import java.util.Map;
import java.util.List;
import java.util.Set;

import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.common.data.Mention;

public abstract class CandidateSelection {
	public abstract Map<Entity, Double> selectCandidates(String name);
	
	public Map<String, Map<Entity, Double>> selectCandidatesFuzzy(String name) {return null;}
	
	public abstract Map<Entity, Double> selectCandidatesName(Set<String> names);
	
	public abstract Map<Mention, Map<Entity, Double>> selectCandidatesMention(List<Mention> mentions);
}