package ca.ualberta.entitylinking.graph.algorithms;

import java.util.List;
import java.util.Map;
import java.util.Set;

import es.yrbcn.graph.weighted.WeightedPageRank;
import it.unimi.dsi.webgraph.ImmutableGraph;

public abstract class PersonalizedPageRank {
	protected ImmutableGraph g = null;
	protected double threshold = 0.00001;	//stopping threshold
	protected int maxIter = 3; 			//maximum iterations.
	protected double alpha = 0.85;
	protected boolean stronglyPreferential = true;
	
	public abstract void init();
	public abstract void setAlpha(double alpha);
	public abstract void setPreference(double[] p);
	public abstract double[] computeRank();
	public abstract Map<Integer, List<Double>> computePageRankParallel(Set<Integer> entities);
	
	public void setStart(double[] s) {}
	
	public static boolean isStochastic(double[] v ) {
		double normL1 = 0.0, c = 0.0, t, y;

		//Kahan method to minimize the round errors in doubles sum.
		for (int i = 0; i < v.length; i++) {
			y = v[i] - c;
			t = ( normL1 + y );
			c = ( t - normL1 ) - y;
			normL1 = t;
		}
		
		return (Math.abs( normL1 - 1.0 ) <=  WeightedPageRank.STOCHASTIC_TOLERANCE );
	}
	

}
