package ca.ualberta.entitylinking.graph;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ca.ualberta.entitylinking.SemanticSignatureEL;
import ca.ualberta.entitylinking.config.ELConfig;


import it.unimi.dsi.webgraph.ASCIIGraph;
import it.unimi.dsi.webgraph.BVGraph;
import it.unimi.dsi.webgraph.ImmutableGraph;

/**
 * Used to create a WebGraph compatible graph from the given data. 
 * @author zhaochen
 *
 */
public class GraphUtils {
	private static Logger LOGGER = LogManager.getLogger(GraphUtils.class);

	/** Default window size. */
	public final static int DEFAULT_WINDOW_SIZE = 7;
	/** Default value of <var>k</var>. */
	public final static int DEFAULT_ZETA_K = 3;
	/** Default backward reference maximum length. */
	public final static int DEFAULT_MAX_REF_COUNT = 3;
	/** Default minimum interval length. */
	public final static int DEFAULT_MIN_INTERVAL_LENGTH = 4;


	public static BVGraph createBVGraph(InputStream input) {
		String basename = "tempDest";

		//Load the graph file, and then convert it to the BVGraph.
		try {
			final ImmutableGraph graph = new ASCIIGraph(input);
			BVGraph.store( graph, basename, DEFAULT_WINDOW_SIZE, 
					DEFAULT_MAX_REF_COUNT, DEFAULT_MIN_INTERVAL_LENGTH, DEFAULT_ZETA_K, 1);
		} catch (Exception e) {e.printStackTrace();}

		//load the BVGraph.
		BVGraph g = null;
		try {
			g = BVGraph.load(basename, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return g;
	}
	
	public static BVGraph createBVGraph(String graphStr) {
		InputStream input = new ByteArrayInputStream(graphStr.getBytes(StandardCharsets.UTF_8));
		return createBVGraph(input);
	}
	
	public static void testGraph(BVGraph graph) {
		System.out.println("numArcs: " + graph.numArcs());
		System.out.println("numNodes: " + graph.numNodes());
		
		for (int i = 0; i < graph.numNodes(); i++) {
			System.out.println("node " + i + " : " + graph.outdegree(i));
			int[] succs = graph.successorArray(i);
			for (int j = 0; j < succs.length; j++)
				System.out.print(" " + succs[j]);
			System.out.println();
		}
	}
	
    /**
     * Build the subgraph around entities from the knowledge base entity graph.
     *
     * @param entities The core component of the subgraph
     * @param e2id A name to id mapping for entities.
     * @return The graphs consisting of edges, each triple is a source, target and weight.
     */
	public static List<Triple> buildWeightedGraph(SubGraphGenerator gg, Set<String> entities,
			Map<String, Integer> e2id) {
		
		List<Triple> graph = null;
		if (ELConfig.directedGraph)
			graph = gg.generateExpandedWeightedDirectedGraph(entities, e2id, ELConfig.expandLevel);
		else
			graph = gg.generateExpandedWeightedUndirectedGraph(entities, e2id, ELConfig.expandLevel);

		if (entities.isEmpty() || graph == null || graph.isEmpty())
			return null;
		
        LOGGER.info("Interested entities: " + entities.size());
        LOGGER.info("Graph nodes: " + e2id.size());
        LOGGER.info("Graph edges: " + graph.size());
        LOGGER.info("edge / node: " + graph.size()*1.0/e2id.size());

		return graph;
	}
	
	public static BVGraph buildUnweightedGraph(SubGraphGenerator gg, Set<String> entities,
			Map<String, Integer> e2id) {
		String graphStr = null;
		if (ELConfig.directedGraph)
			graphStr = gg.generateExpandedDirectedGraph(entities, e2id, ELConfig.expandLevel);			
		else
			graphStr = gg.generateExpandedUndirectedGraph(entities, e2id, ELConfig.expandLevel);

		BVGraph graph = createBVGraph(graphStr); 
        LOGGER.info("Interested entities: " + entities.size());
        LOGGER.info("Graph nodes: " + graph.numNodes());
        LOGGER.info("Graph edges: " + graph.numArcs());
        LOGGER.info("edge / node: " + graph.numArcs()*1.0/e2id.size());
        
        return graph;
	}


	
	public static void main(String[] args) {
		String eol = System.getProperty("line.separator");
		System.out.println(eol);
		String graphStr = "6" + eol +
				"1 2 3" + eol +
				"4 3 2" + eol +
				"5 4 3" + eol +
				"" + eol +
				"" + eol +
				"" + eol;
		
		
//		InputStream input = new ByteArrayInputStream(graphStr.getBytes(StandardCharsets.UTF_8));
		InputStream input = null;
		try {
			input = new FileInputStream("sample.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BVGraph graph = createBVGraph(input);
		testGraph(graph);
	}
}
