package ca.ualberta.entitylinking.graph.similarity.context;

import ca.ualberta.entitylinking.common.data.Document;
import ca.ualberta.entitylinking.common.data.Mention;
import ca.ualberta.entitylinking.common.data.Sentence;
import ca.ualberta.entitylinking.common.data.Token;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;
import ca.ualberta.entitylinking.common.indexing.Tokenizer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhaochen on 22/09/14.
 */
public class MentionContext {
    public static int CTX_WIN_SIZE = 25;

    public static Map<String, Float> getContext(String ctxText,
                                                Tokenizer tokener, TFIDF3x index) {
        Map<String, Float> ret = new HashMap<String, Float>();

        List<Tokenizer.Token> contexts = tokener.extractContext(ctxText);
        Map<String, Integer> termFreq = new HashMap<String, Integer>();

        for (Tokenizer.Token tok : contexts) {
            String term = tok.text.toLowerCase();

            int freq = 1;
            if (termFreq.containsKey(term))
                freq += termFreq.get(term);
            termFreq.put(term, freq);
        }

        for (String term : termFreq.keySet()) {
            int tf = termFreq.get(term);
            float termScore = index.computeTFIDF(term, tf);
            ret.put(term, termScore);
        }

        return ret;
    }
    /**
     * Simply use the sentence containing the mention as the context text.
     * @param m
     * @return Context text.
     */
    public static String mentionContextText(Mention m) {
        Document doc = m.getChunk().getSentence().getDocument();
        List<Sentence> sentences = doc.getSentences();
        Sentence sentence = m.getChunk().getSentence();

        int sentNum = sentence.getNumber();
        List<Token> tokens = sentence.getTokens();

        int sTok = m.getChunk().getStartToken();
        int eTok = m.getChunk().getEndToken();

        String context = "";
        if (sTok < CTX_WIN_SIZE) {
            //previous sentence
            if (sentNum == 0) // first sentence
                context = "";
            else
                context = sentences.get(sentNum-1).getText();
        }

        // current sentence.
        context = context + " " + sentence.getText();

        if (eTok + CTX_WIN_SIZE > tokens.size()) {
            //next sentence
            if (sentNum < sentences.size()-1) //ignore if this is the last sentence
                context = context + " " + sentences.get(sentNum+1).getText();
        }

        return context;
    }


}
