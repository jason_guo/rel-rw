package ca.ualberta.entitylinking.graph.similarity.context;

import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhaochen on 22/09/14.
 */
public class EntityContextCache {
    private TFIDF3x index = null;

    //Cache the context information of entities for efficiency
    private Map<Entity, Map<String, Float>> entCtxMap = new HashMap<Entity, Map<String, Float>>();

    public EntityContextCache() {
    }

    public EntityContextCache(TFIDF3x index) {
        this.index = index;
    }

    public Map<String, Float> getContext(Entity e) {
        if (entCtxMap == null) return null;

        if (entCtxMap.containsKey(e))
            return entCtxMap.get(e);

        Map<String, Float> context = index.DocTFIDFVector(e.getName());
        entCtxMap.put(e, context);

        return context;
    }
    
    public void clear() {
    	entCtxMap.clear();
    }
}
