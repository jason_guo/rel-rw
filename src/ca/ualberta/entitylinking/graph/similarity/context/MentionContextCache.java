package ca.ualberta.entitylinking.graph.similarity.context;

import ca.ualberta.entitylinking.common.data.Mention;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;
import ca.ualberta.entitylinking.common.indexing.Tokenizer;
import ca.ualberta.entitylinking.config.ELConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhaochen on 22/09/14.
 */
public class MentionContextCache {
    private int ctx_option = ELConfig.DOC_CTX;

    private Tokenizer tokener = null;
    private TFIDF3x index = null;

    private Map<String, Float> docCtx = null;
    private Map<Mention, Map<String, Float>> mentionCtxMap = new HashMap<Mention, Map<String, Float>>();

    public MentionContextCache() {
    }

    public MentionContextCache(int option, Tokenizer tokener, TFIDF3x index) {
        this.ctx_option = option;
        this.tokener = tokener;
        this.index = index;
    }

    public Map<String, Float> getContext(Mention m) {
        Map<String, Float> context = null;
        if (ctx_option == ELConfig.DOC_CTX) {
            if (docCtx == null) {
                String content = m.getSentence().getDocument().getOriginalText();
                docCtx = MentionContext.getContext(content, tokener, index);
            }

            context = docCtx;
        } else {
            if (mentionCtxMap.containsKey(m)) {
                context = mentionCtxMap.get(m);
            } else {
                String content = MentionContext.mentionContextText(m);
                context = MentionContext.getContext(content, tokener, index);
                mentionCtxMap.put(m, context);
            }
        }

        return context;
    }
    
    public void clear() {
    	docCtx = null;
    	mentionCtxMap.clear();
    }
}
