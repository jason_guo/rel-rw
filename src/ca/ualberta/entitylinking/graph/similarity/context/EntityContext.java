package ca.ualberta.entitylinking.graph.similarity.context;

import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.common.indexing.TFIDF3x;

import java.util.Map;

/**
 * Created by zhaochen on 22/09/14.
 */
public class EntityContext {
    public static Map<String, Float> getEntityContext(Entity e, TFIDF3x index) {
        return index.DocTFIDFVector(e.getName());
    }
}
