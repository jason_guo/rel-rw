package ca.ualberta.entitylinking.graph.similarity.measure;

import java.util.Map;

import ca.ualberta.entitylinking.common.data.Mention;
import ca.ualberta.entitylinking.common.data.Entity;
import ca.ualberta.entitylinking.graph.similarity.context.EntityContextCache;
import ca.ualberta.entitylinking.graph.similarity.context.MentionContextCache;
import ca.ualberta.entitylinking.utils.similarity.VectorSimilarity;


/**
 * Created by zhaochen on 22/09/14.
 */
public class SimilarityMeasure {
    public static double mentionEntitySimilarity(Mention m, Entity e,
                                          MentionContextCache mentionCache,
                                          EntityContextCache entityCache) {
        //get mention context.
        Map<String, Float> mentionCtx = mentionCache.getContext(m);

        //get entity context.
        Map<String, Float> entityCtx = entityCache.getContext(e);

        return VectorSimilarity.vectorSim(mentionCtx, entityCtx);
    }
}
