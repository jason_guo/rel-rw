package ca.ualberta.entitylinking.graph.similarity.measure;

public interface RelatednessMeasure {
	public double semanticRelatedness(String e1, String e2);
}
