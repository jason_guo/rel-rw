package ca.ualberta.entitylinking.graph;

/**
 * Represent each edge in a graph using a triple (source, target, edge).
 * 
 * @author zhaochen
 *
 */
public class Triple {
	public int s, t;
	public double w;
	
	public Triple(int s, int t, double w) {
		this.s = s;
		this.t = t;
		this.w = w;
	}
}
